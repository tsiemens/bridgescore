package com.tsiemens.bridgescore.api

import com.tsiemens.bridgescore.Trace
import com.tsiemens.bridgescore.funcTag
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class Response(val code: Int, val data: String?) {
    fun isOk(): Boolean = code < 400
}

fun requestData(host: String, path: String, data: String?=null, tls: Boolean=true): Response? {
    val protocol = if(tls) "https" else "http"
    Trace.v(funcTag(), "Requesting $protocol://$host$path, $data")

    var conn: HttpURLConnection? = null
    try {
        val method = if(data == null) "GET" else "POST"

        val url = URL(protocol + "://" + host + path)
        conn = url.openConnection() as HttpURLConnection
        conn.requestMethod = method
        conn.useCaches = false
        conn.doInput = true
        conn.connectTimeout = 5000
        if (data != null) {
            conn.setRequestProperty("Content-Type", "application/json")
            conn.setRequestProperty("Content-Length", data.toByteArray().size.toString())
            conn.doOutput = true
            conn.outputStream.write(data.toByteArray())
            conn.outputStream.flush()
            conn.outputStream.close()
        } else {
            conn.doOutput = false
        }

        Trace.v(funcTag(), "Received ${conn.responseCode}")
        val reader = BufferedReader(InputStreamReader(conn.inputStream))
        val response = reader.readText()
        reader.close()
        Trace.v(funcTag(), "Data received: \"$response\"")
        return Response(conn.responseCode, response)
    } catch (e: Exception) {
        Trace.i(funcTag(), "Request failed:", e)
    } finally {
        if(conn != null) {
            conn.disconnect()
        }
    }
    return null
}

fun requestDataWithFallback(host: String, path: String, data: String?=null): Response? {
    var resp = requestData(host, path, data, true)
    if (resp == null) {
        resp = requestData(host, path, data, false)
    }
    return resp
}

fun getSessions(host: String): List<ApiSession>? {
    val resp = requestDataWithFallback(host, "/sessions")
    if(resp == null || !resp.isOk() || resp.data == null) {
        return null
    }

    return getFromJson(resp.data, { json ->
        val sessionsJson = json.getJSONArray("Sessions")
        val sessions = ArrayList<ApiSession>()
        for(i in  0 .. sessionsJson.length() - 1) {
            sessions.add(sessionFromJson(sessionsJson.getJSONObject(i)))
        }
        sessions
    })
}

fun newSession(host: String, serverPassword: String?, session: ApiSession): Long? {
    val json = JSONObject()
    if(serverPassword != null) {
        json.put("Password", serverPassword)
    }
    val sessionJson = JSONObject()
    sessionJson.put("Name", session.name)
    sessionJson.put("Timestamp", session.timestamp)
    json.put("Session", sessionJson)

    val resp = requestDataWithFallback(host, "/newsession", data = json.toString())
    if(resp == null || !resp.isOk() || resp.data == null) {
        return null
    }

    return getFromJson(resp.data, { json ->
        json.getLong("SessionId")
    })
}

fun delSession(host: String, serverPassword: String?, sessionId: Long): Boolean {
    val json = JSONObject()
    if(serverPassword != null) {
        json.put("Password", serverPassword)
    }
    json.put("SessionId", sessionId)

    val resp = requestDataWithFallback(host, "/delsession", data = json.toString())
    return resp != null && resp.isOk()
}

fun getBoardsSets(host: String, sessionId: Long): List<ApiBoardSet>? {
    val resp = requestDataWithFallback(host, "/boards?sessionId=" + sessionId.toString())
    if(resp == null || !resp.isOk() || resp.data == null) {
        return null
    }

    return getFromJson(resp.data, { json ->
        val boardSetsJson = json.getJSONArray("BoardSets")
        val boardSets = ArrayList<ApiBoardSet>()
        for(i in  0 .. boardSetsJson.length() - 1) {
            boardSets.add(boardSetFromJson(boardSetsJson.getJSONObject(i)))
        }
        boardSets
    })
}

fun updateBoardSet(host: String, serverPassword: String?, sessionId: Long,
                   boardSet: ApiBoardSet, pubId: String): Boolean {
    val json = JSONObject()
    if(serverPassword != null) {
        json.put("Password", serverPassword)
    }
    json.put("SessionId", sessionId)
    json.put("BoardSet", boardSetToJson(boardSet.boardSet, pubId, boardSet.boards))

    val resp = requestDataWithFallback(host, "/boards", data = json.toString())
    return resp != null && resp.isOk()
}