package com.tsiemens.bridgescore.api

import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import com.tsiemens.bridgescore.orm.BridgeBoardSet
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

enum class ApiSuit {
    none,
    noTrumps,
    spades,
    hearts,
    diamonds,
    clubs
}

enum class ApiDirection {
    none,
    north,
    south,
    east,
    west
}

enum class ApiDoubled {
    undoubled,
    doubled,
    redoubled
}

class ApiSession(var id: Long, val name: String, val timestamp: Long)

class ApiBoardSet(val boardSet: BridgeBoardSet, val boards: List<BridgeBoard>)

fun <T> getFromJson(jsonStr: String, getter: (json: JSONObject) -> T ): T? {
    try {
        return getter(JSONObject(jsonStr))
    } catch(e: JSONException) {
        Trace.e(funcTag(), "Failed to parse json", e)
        return null
    }
}

fun sessionFromJson(json: JSONObject): ApiSession {
    try {
        return ApiSession(
                id = json.getLong("Id"),
                name = json.getString("Name"),
                timestamp = json.getLong("Timestamp")
        )
    } catch(e: KotlinNullPointerException) {
        throw JSONException(e.toString())
    }
}

/** Throws JSONException
 * */
fun boardFromJson(json: JSONObject): BridgeBoard {
    try {
        val board = BridgeBoard(
                boardNumber = json.getInt("BoardNumber"),
                suit = Suit.valueOf(ApiSuit.valueOf(json.getString("Suit")).name),
                level = json.getInt("Level"),
                doubled = Doubled.valueOf(ApiDoubled.valueOf(json.getString("Doubled")).name),
                tricks = json.getInt("Tricks"),
                declarer = Seat.valueOf(ApiDirection.valueOf(json.getString("Declarer")).name),
                mySeat = Seat.valueOf(ApiDirection.valueOf(json.getString("PublisherSeat")).name)
        )
        board.boardSetId = 0 // Just to pass isValid
        if(!board.isValid()) {
            throw JSONException("Invalid board values")
        }
        board.boardSetId = null
        return board
    } catch(e: IllegalArgumentException) {
      throw JSONException(e.toString())
    }
}

fun boardToJson(board: BridgeBoard): JSONObject {
    val json = JSONObject()
    json.put("BoardNumber", board.boardNumber)
    json.put("Suit", ApiSuit.valueOf(board.suit.name).name)
    json.put("Level", board.level)
    json.put("Doubled", ApiDoubled.valueOf(board.doubled.name).name)
    json.put("Tricks", board.tricks)
    json.put("Declarer", ApiDirection.valueOf(board.declarer.name).name)
    json.put("PublisherSeat", ApiDirection.valueOf(board.mySeat.name).name)
    return json
}

/** Throws JSONException
 * */
fun boardSetFromJson(json: JSONObject): ApiBoardSet {
    val set = BridgeBoardSet(
            teamName = json.getString("Team"),
            publisherId = json.getString("PublisherId")
    )
    set.sessionId = 0 // Just to pass isValid
    if(!set.isValid()) {
        throw JSONException("Invalid board set values")
    }
    set.sessionId = null
    val boards = ArrayList<BridgeBoard>()
    val boardsJson = json.getJSONArray("Boards")
    for(i in  0 .. boardsJson.length() - 1) {
        boards.add(boardFromJson(boardsJson.getJSONObject(i)))
    }
    return ApiBoardSet(set, boards)
}

fun boardSetToJson(set: BridgeBoardSet, pubId: String, boards: List<BridgeBoard>): JSONObject {
    val json = JSONObject()
    json.put("Team", set.teamName)
    json.put("PublisherId", pubId)
    val boardJsonObjs = ArrayList<JSONObject>()
    for(board in boards) {
        boardJsonObjs.add(boardToJson(board))
    }
    val boardsJsonArray = JSONArray(boardJsonObjs)
    json.put("Boards", boardsJsonArray)
    return json
}
