package com.tsiemens.bridgescore

import android.util.Log
import java.util.*

val TRACE_ENABLED = true
val RING_SIZE = 100
val traceBuffers = initBuffers()

enum class TraceLevel {
    error,
    warning,
    info,
    verbose,
    debug
}

class TraceRingArray(val cap: Int) {
    val list = ArrayList<Pair<Long, String>>(cap)
    var nextIdx = 0

    fun add(str: String) {
        val entry = Pair(System.nanoTime(), str)
        if(list.size < cap) {
            list.add(entry)
        } else {
            if(nextIdx == cap) {
                nextIdx = 0
            }
            list[nextIdx] = entry
        }
        nextIdx++
    }
}

private fun initBuffers(): Map<TraceLevel, TraceRingArray> {
    val bufs = HashMap<TraceLevel, TraceRingArray>(5)
    for(l in TraceLevel.values()) {
        bufs[l] = TraceRingArray(RING_SIZE)
    }
    return bufs
}

class Trace {
    companion object {
        fun trace(level: TraceLevel, tag: String, msg: String, e: Exception? = null) {
            traceBuffers[level]!!.add(tag + " : " + msg + if (e != null) "\n" + e.toString() else "")
        }

        fun e(tag: String, msg: String, e: Exception? = null) {
            if (e == null) {
                Log.e(tag, msg)
            } else {
                Log.e(tag, msg, e)
            }
            trace(TraceLevel.error, tag, msg, e)
        }

        fun w(tag: String, msg: String, e: Exception? = null) {
            if (e == null) {
                Log.w(tag, msg)
            } else {
                Log.w(tag, msg, e)
            }
            trace(TraceLevel.warning, tag, msg, e)
        }

        fun i(tag: String, msg: String, e: Exception? = null) {
            if (e == null) {
                Log.i(tag, msg)
            } else {
                Log.i(tag, msg, e)
            }
            trace(TraceLevel.info, tag, msg, e)
        }

        fun v(tag: String, msg: String, e: Exception? = null) {
            if (e == null) {
                Log.v(tag, msg)
            } else {
                Log.v(tag, msg, e)
            }
            trace(TraceLevel.verbose, tag, msg, e)
        }

        fun d(tag: String, msg: String, e: Exception? = null) {
            if (e == null) {
                Log.d(tag, msg)
            } else {
                Log.d(tag, msg, e)
            }
            trace(TraceLevel.debug, tag, msg, e)
        }
    }
}
