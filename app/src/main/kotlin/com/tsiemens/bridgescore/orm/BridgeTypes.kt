package com.tsiemens.bridgescore.orm

import com.orm.dsl.Table
import com.tsiemens.bridgescore.*

@Table
class BridgeBoard(var boardNumber: Int = 0,
                  var suit: Suit = Suit.none,
                  var level: Int = -1,
                  var doubled: Doubled = Doubled.undoubled,
                  var tricks: Int = 0,
                  var declarer: Seat = Seat.none,
                  var mySeat: Seat = Seat.none,
                  var honors: Honors = Honors.none,
                  var nsHighCardPoints: Int = 0,
                  var scoringLineFitLength: Int = 0,
                  var scoringLineFitSuit: Suit = Suit.none,
                  var boardSetId: Long? = null,
                  var notes: String? = null
                  ) : OrmRecord() {
    var id: Long? = null
    override fun getid(): Long? = id
    override fun setid(v: Long?){ id = v }

    fun relativeTricks(): Int {
        return tricks - (6 + level)
    }

    fun playerIsNorthSouth(): Boolean {
        return mySeat == Seat.north || mySeat == Seat.south
    }

    override fun isValid(): Boolean {
        if(boardNumber <= 0 || level < 0 || boardSetId == null || mySeat == Seat.none ||
                tricks < 0 || nsHighCardPoints > 40 || nsHighCardPoints < 0 ||
                scoringLineFitLength < 0 || scoringLineFitLength > 13 || (scoringLineFitLength > 0 && scoringLineFitLength < 8) ||
                scoringLineFitSuit == Suit.noTrumps) {
            return false
        }
        if(level == 0) {
            return suit == Suit.none && level == 0 && doubled == Doubled.undoubled &&
                    tricks == 0 && declarer == Seat.none
        } else {
            return suit != Suit.none && level <= 7 &&
                    tricks <= 13 && declarer != Seat.none
        }
    }

    override fun toString(): String {
        val honorsStr = if(honors != Honors.none) "plus " + honors.name + " honors" else ""
        return String.format(
                "BridgeBoard(%d, %d %s %s (dclr: %s) - %d tricks made %s- played from %s - " +
                "HCP: %d, fit: %d %s)",
                boardNumber, level, suit, doubled, declarer, tricks,
                honorsStr, mySeat, nsHighCardPoints, scoringLineFitLength, scoringLineFitSuit)
    }

    fun boardEquals(other: BridgeBoard?): Boolean {
        if (other == null) return false
        return (boardNumber == other.boardNumber &&
                suit == other.suit &&
                level == other.level &&
                doubled == other.doubled &&
                tricks == other.tricks &&
                declarer == other.declarer &&
                mySeat == other.mySeat &&
                honors == other.honors &&
                nsHighCardPoints == other.nsHighCardPoints &&
                scoringLineFitLength == other.scoringLineFitLength &&
                scoringLineFitSuit == other.scoringLineFitSuit
                )
    }
}

@Table
class BridgeBoardSet(var teamName: String = "", // Set only when a sync session is configured
                     var sessionId: Long? = null,
                     var publisherId: String? = null // null for the local set
                     ) : OrmRecord() {

    var id: Long? = null
    override fun getid(): Long? = id
    override fun setid(v: Long?){ id = v }

    override fun isValid(): Boolean {
        return sessionId != null
    }

    companion object {
        fun boardByNumber(boardSetId: Long, boardNo: Int): BridgeBoard? {
            val boardList = Orm.find<BridgeBoard>(BridgeBoard::class.java,
                    "board_set_id = ? AND board_number = ?", boardSetId.toString(), boardNo.toString())

            return if(boardList.size > 0) boardList[0] else null
        }
    }

    fun boardWithNumber(boardNo: Int): BridgeBoard? = boardByNumber(id!!, boardNo)

    fun isMine(): Boolean {
        return publisherId == null;
    }

    fun boards(): List<BridgeBoard> {
        return Orm.find(BridgeBoard::class.java, "board_set_id = ?", this.id.toString())
    }

    fun boardCount(): Long {
        val args = Array<String>( 1, { i -> id.toString() } )
        return Orm.count<BridgeBoard>(BridgeBoard::class.java, "board_set_id = ?", args)
    }
}

enum class SessionSyncMode {
    noSync,
    pushPull,
    pullOnly
}

@Table
class BridgeSession(var name: String = "", // Used only when a remote session is configured
                    var mySeatDefault: Seat = Seat.none,
                    var boardsPerRound: Int = 1,
                    var timestamp: Long = 0,
                    var type: ContractBridgeType = ContractBridgeType.invalid,
                    var syncGuid: Long? = null,
                    var syncMode: SessionSyncMode = SessionSyncMode.noSync,
                    var syncServer: String? = null,
                    var syncServerPassword: String? = null,
                    // used only for teams games. Contains the publisher id of the other pair
                    // on the team, which will be compared against.
                    var teammatePublisherId: String? = null,
                    // save the most recently edit board number (BridgeBoard.boardNumber)
                    var lastEditBoard: Int = 0
                    ) : OrmRecord() {
    var id: Long? = null
    override fun getid(): Long? = id
    override fun setid(v: Long?){ id = v }

    fun boardSets(): List<BridgeBoardSet> {
        return Orm.find(BridgeBoardSet::class.java, "session_id = ?", this.id.toString())
    }

    fun myBoardSet(): BridgeBoardSet {
        val sets = Orm.find(BridgeBoardSet::class.java, "session_id = ? AND publisher_id is NULL",
                this.id.toString())
        doAssert(sets.size == 1)
        return sets[0]
    }

    override fun isValid(): Boolean {
        return mySeatDefault != Seat.none && boardsPerRound > 0 &&
                type != ContractBridgeType.invalid
    }
}