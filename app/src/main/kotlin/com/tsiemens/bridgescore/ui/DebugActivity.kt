package com.tsiemens.bridgescore.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tsiemens.bridgescore.R
import com.tsiemens.bridgescore.RING_SIZE
import com.tsiemens.bridgescore.TraceLevel
import com.tsiemens.bridgescore.traceBuffers
import java.util.*

class DebugActivity : AppCompatActivity() {

    var logEntries = ArrayList<LogEntry>(RING_SIZE * 5)
    var listAdapter: TraceListAdapter = TraceListAdapter(logEntries)

    var recycler: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debug)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        recycler = findViewById(R.id.list)
        recycler!!.setHasFixedSize(true)
        val layoutMgr = LinearLayoutManager(this)
        recycler!!.layoutManager = layoutMgr
        recycler!!.adapter = listAdapter

        updateListAdapter()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item!!.itemId) {
            android.R.id.home -> finish()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    fun updateListAdapter() {
        logEntries.clear()
        for((level, buf) in traceBuffers) {
            for(log in buf.list) {
                logEntries.add(LogEntry(level, log.first, log.second))
            }
        }
        logEntries.sortBy { e -> e.timestamp }
        listAdapter.notifyDataSetChanged()
        recycler!!.scrollToPosition(logEntries.size - 1)
    }

    class LogEntry(val level: TraceLevel, val timestamp: Long, val msg: String)

    class ViewHolder(iv: View) : RecyclerView.ViewHolder(iv) {}

    inner class TraceListAdapter(list: List<LogEntry>) :
            ListAdapter<LogEntry, ViewHolder>(list) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent!!.context)
                    .inflate(R.layout.debug_log_list_item, null)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val logEntry = list[position]
            getTextView(holder, R.id.tv_timestamp).text = logEntry.timestamp.toString()
            getTextView(holder, R.id.tv_log_msg).text = logEntry.msg

            val color = when(logEntry.level) {
                TraceLevel.error -> R.color.logBgError
                TraceLevel.warning -> R.color.logBgWarning
                TraceLevel.info -> R.color.logBgInfo
                TraceLevel.verbose -> R.color.logBgVerbose
                TraceLevel.debug -> R.color.logBgDebug
            }

            holder!!.itemView.findViewById<View>(R.id.item_bg).setBackgroundColor(
                    ContextCompat.getColor(holder.itemView.context, color)
            )
        }



    }
}
