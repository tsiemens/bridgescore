package com.tsiemens.bridgescore.ui

import android.os.Bundle
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import java.util.*

class RubberBoardListUiState : BoardListUiState() {
    /**
     * Get the expected next board number to be created.
     * In rubber, we can assume it is just the last board + 1
     */
    override fun predictNextBoardNo(): Int? {
        val lastBoard = lastBoardNumber()
        if(lastBoard != null) {
            return lastBoard + 1
        } else {
            return 1
        }
    }
}

class RubberBoardListActivity : BoardListActivity() {

    override fun makeUiState(): BoardListUiState {
        return RubberBoardListUiState()
    }

    override fun contentLayoutId() = R.layout.activity_rubber_board_list

    var scoreboard: RubberScoreboard? = null

    var fragPager: ViewPager? = null
    var pagerAdapter: ViewPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragPager = findViewById(R.id.view_pager)
        pagerAdapter = ViewPagerAdapter(supportFragmentManager)
        pagerAdapter!!.addFrag(BoardListFragment(), getString(R.string.rubber_tab_boards))
        pagerAdapter!!.addFrag(RubberScoreboardFragment(), getString(R.string.rubber_tab_scoreboard))
        fragPager!!.adapter = pagerAdapter!!
        fragPager!!.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                val fab: FloatingActionButton = findViewById(R.id.fab)
                when(state) {
                    ViewPager.SCROLL_STATE_DRAGGING -> fab.hide()
                    ViewPager.SCROLL_STATE_SETTLING -> {}
                    ViewPager.SCROLL_STATE_IDLE -> {
                        val lp = fab.layoutParams as CoordinatorLayout.LayoutParams
                        lp.anchorId = if(fragPager!!.currentItem == 0) R.id.list
                            else R.id.sv_scoreboard
                        fab.layoutParams = lp
                        fab.show()
                    }
                    else -> throw IllegalStateException(state.toString())
                }
            }

            override fun onPageScrolled(position: Int, positionOffset: Float,
                                        positionOffsetPixels: Int) {
                if(positionOffset < 0.9999f && positionOffset > 0.0001f) {
                    val fab: FloatingActionButton = findViewById(R.id.fab)
                    fab.hide()
                }
            }

            override fun onPageSelected(position: Int) {}

        })

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(fragPager!!)
    }

    override fun onResume() {
        super.onResume()
        updateBoardList()
    }

    override fun updateAllViews() {
        updateBoardList()
    }

    fun updateBoardList() {
        boardList.clear()
        boardList.addAll(uiState.nonDeletedBoards())
        sortBoardList()

        scoreboard = rubberScoreboard(boardList)

        notifyBoardsListChanged()
    }

    override fun boardIsVulnerable(board: BridgeBoard): Boolean {
        return scoreboard!!.boardVuln[board.boardNumber] ?: false
    }

    override fun boardDisplayScore(board: BridgeBoard): Int? = null

    override  fun getContractType(): ContractBridgeType = ContractBridgeType.rubber

    inner class ViewPagerAdapter(mgr: FragmentManager) : FragmentPagerAdapter(mgr) {
        val fragments = ArrayList<BaseBoardListFragment>();
        val fragmentTitles = ArrayList<String>();

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int {
            return fragments.size
        }

       fun addFrag(frag: BaseBoardListFragment, title: String) {
           fragments.add(frag)
           fragmentTitles.add(title)
       }

        @Override
        override fun getPageTitle(position: Int): CharSequence {
            return fragmentTitles[position];
        }
    }

    /**
     * Handles deletions of boards from fragments to notify that the boardsList has been modified
     * */
    override fun notifyBoardsListChanged() {
        // If frag is not resumed, this will happen automatically when it does
        for(frag in pagerAdapter!!.fragments) {
            if (frag.isResumed) {
                frag.updateBoardsView()
            }
        }
    }
}
