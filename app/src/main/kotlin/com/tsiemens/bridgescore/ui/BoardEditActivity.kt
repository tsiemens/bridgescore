package com.tsiemens.bridgescore.ui

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.orm.SugarRecord
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import com.tsiemens.bridgescore.orm.BridgeBoardSet
import com.tsiemens.bridgescore.orm.BridgeSession
import com.tsiemens.bridgescore.orm.Orm
import java.util.*

class BoardEditActivity : AppCompatActivity() {

    companion object Consts {
        val MY_SEAT_KEY = "my_seat"
        val BOARD_SET_ID = "board_set_id" // required
        val BOARD_ID = "board_id"
        val DEFAULT_BOARD_NUMBER = "default_board_number"
        val CONTRACT_TYPE = "contract_type"
        val SESSION_ID = "session_id"

        private val MAX_BOARD_NUM = 99
        private val MAX_HCP = 40
        private val MIN_HCP = 0
    }

    private enum class MadeOrDown {
        invalid, made, down
    }

    var boardSetId: Long = -1
    var boardId: Long? = null
    var sessionId: Long? = null

    var mySeatHelper: SeatSelectionHelper? = null
    var declarerSeatHelper: SeatSelectionHelper? = null

    var tricksList: ArrayList<Int> = ArrayList(13)
    var tricksSpinnerAdapter: ArrayAdapter<Int>? = null
    var tricksSpinner: Spinner? = null

    var vulnerableTv: TextView? = null
    var dealerTv: TextView? = null

    var honorsSpinner: Spinner? = null

    var contractType = ContractBridgeType.invalid

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_board_edit)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        boardSetId = intent.getLongExtra(BOARD_SET_ID, -1)
        doAssert(boardSetId != -1L)

        boardId = intent.getLongExtra(BOARD_ID, -1)
        boardId = if(boardId == -1L) null else boardId

        sessionId = if(intent.hasExtra(SESSION_ID)) intent.getLongExtra(SESSION_ID, -1) else null

        contractType = intent.getSerializableExtra(CONTRACT_TYPE) as ContractBridgeType
        doAssert(contractType != ContractBridgeType.invalid)

        vulnerableTv = findViewById(R.id.tv_vul)
        vulnerableTv!!.visibility = when ( contractType ) {
            ContractBridgeType.duplicate,
            ContractBridgeType.teams,
            ContractBridgeType.parScore
                -> View.VISIBLE
            ContractBridgeType.rubber
                -> View.GONE
            ContractBridgeType.invalid
                -> throw IllegalStateException()
        }

        dealerTv = findViewById(R.id.tv_dealer)

        val boardNoEt: EditText = findViewById(R.id.et_board)
        boardNoEt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                updateVulnerableView()
                updateDealerView()
                updateScoringLinePair()
                updateParScore()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })

        val initMySeat = Seat.valueOf(intent.getStringExtra(MY_SEAT_KEY) ?: Seat.none.name)
        val mySeatRg: RadioGroup = findViewById(R.id.rg_my_seat)

        if(initMySeat != Seat.none) {
            mySeatRg.check(when (initMySeat) {
                Seat.none -> throw AssertionError()
                Seat.north -> R.id.rb_north
                Seat.east -> R.id.rb_east
                Seat.south -> R.id.rb_south
                Seat.west -> R.id.rb_west
            })
        }

        findViewById<TextView>(R.id.tv_my_seat)!!.setOnClickListener { v -> setMySeatControlsVisible(true) }
        for (i in 0 .. mySeatRg.childCount - 1) {
            mySeatRg.getChildAt(i).setOnClickListener { v ->
                setMySeatControlsVisible(false)
            }
        }

        mySeatHelper = SeatSelectionHelper(findViewById(R.id.rg_my_seat),
                R.id.rb_north, R.id.rb_east, R.id.rb_south, R.id.rb_west)
        setMySeatControlsVisible(initMySeat == Seat.none)

        val declarerRg: RadioGroup = findViewById(R.id.rg_declarer)
        declarerSeatHelper = SeatSelectionHelper(declarerRg,
                R.id.rb_north2, R.id.rb_east2, R.id.rb_south2, R.id.rb_west2)
        declarerRg.setOnCheckedChangeListener { radioGroup, i ->
            updateParScore()
        }

        tricksSpinner = findViewById(R.id.spinner_tricks)
        tricksSpinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,
                tricksList)
        tricksSpinnerAdapter!!.setDropDownViewResource(R.layout.simple_spinner_item)

        tricksSpinner!!.adapter = tricksSpinnerAdapter
        updateTricksSpinner()

        findViewById<RadioGroup>(R.id.rg_level).setOnCheckedChangeListener { radioGroup, i ->
            updateTricksSpinner()
        }
        findViewById<RadioGroup>(R.id.rg_made).setOnCheckedChangeListener { radioGroup, i ->
            updateTricksSpinner()
        }

        // Honors
        findViewById<View>(R.id.ll_honors)!!.visibility =
                if(contractType == ContractBridgeType.rubber) View.VISIBLE else View.GONE

        honorsSpinner = findViewById(R.id.spinner_honors)
        val honorsList = Honors.values().map { honors -> getString(honors.strRes) }
        val honorsSpinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,
                honorsList)
        honorsSpinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item)
        honorsSpinner!!.adapter = honorsSpinnerAdapter

        // Notes
        findViewById<View>(R.id.ll_board_notes)!!.visibility =
                if(contractType == ContractBridgeType.duplicate ||
                   contractType  == ContractBridgeType.teams) View.VISIBLE else View.GONE

        // Scoring line
        findViewById<View>(R.id.ll_scoring_line)!!.visibility =
              if(contractType == ContractBridgeType.parScore) View.VISIBLE else View.GONE
        findViewById<EditText>(R.id.et_ns_hcp).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                updateScoringLinePair()
                updateParScore()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        findViewById<RadioGroup>(R.id.rg_sl_fit_len).setOnCheckedChangeListener { radioGroup, i ->
            updateFitSuitRg()
            updateParScore()
        }
        findViewById<RadioGroup>(R.id.rg_sl_fit_suit).setOnCheckedChangeListener { radioGroup, i ->
            updateFitSuitRg()
            updateParScore()
        }

        setValuePresets()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater;
        inflater.inflate(R.menu.board_edit_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item!!.itemId) {
            android.R.id.home -> finish()
            R.id.save -> saveAndQuit()
            R.id.no_bid -> showNoBidDialog()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onResume() {
        super.onResume()

        // The board number has been populated, so don't open the keyboard by default
        if(!(findViewById<EditText>(R.id.et_board)).text.isEmpty()) {
            // Hide the keyboard (hacky, yes, but it works)
            val handler = Handler()
            handler.postDelayed({ ->
                val focus = this.currentFocus;
                if (focus != null) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(focus.windowToken, 0)
                }
            }, 50)
        }
    }

    private fun updateVulnerableView() {
        val boardNo = getBoardNumber()
        val vul = if(boardNo != null && boardNo > 0)
            duplicateBoardVulnerability(boardNo) else null
        vulnerableTv!!.text = fmtString(this, R.string.board_editor_vul_fmt,
                getString(when(vul) {
                    Vulnerability.both -> R.string.vul_both
                    Vulnerability.ns -> R.string.vul_ns
                    Vulnerability.ew -> R.string.vul_ew
                    Vulnerability.none -> R.string.none
                    else -> R.string.dash
                })
        )
    }

    private fun updateDealerView() {
        val boardNo = getBoardNumber()
        val dealer: String
        if (boardNo == null || boardNo <= 0) {
            dealer = "-"
        } else {
            dealer = getString(when(boardNo % 4) {
                1 -> R.string.north_abv
                2 -> R.string.east_abv
                3 -> R.string.south_abv
                0 -> R.string.west_abv
                else -> throw AssertionError(boardNo)
            })
        }
        dealerTv!!.text = fmtString(this, R.string.board_editor_dealer_fmt, dealer)
    }

    private fun setMySeatControlsVisible(vis: Boolean) {
        val mySeatTv: TextView = findViewById(R.id.tv_my_seat)
        val mySeatRg: RadioGroup = findViewById(R.id.rg_my_seat)

        if(vis) {
            mySeatTv.visibility = View.INVISIBLE
            mySeatRg.visibility = View.VISIBLE
        } else {
            val selectedSeat = getMySeat()
            mySeatTv.text = fmtString(this, R.string.my_seat_colon_fmt, selectedSeat.name)
            mySeatTv.visibility = View.VISIBLE
            mySeatRg.visibility = View.INVISIBLE
        }
    }

    private fun setValuePresets() {
        val boardNoEt: EditText = findViewById(R.id.et_board)
        boardNoEt.text.clear()
        if(boardId == null) {
            val defBoardNo = intent.getIntExtra(DEFAULT_BOARD_NUMBER, -1)
            if(defBoardNo != -1) {
                boardNoEt.text.append(defBoardNo.toString())
            } else {
                boardNoEt.requestFocus()
            }
            return
        }

        val board = SugarRecord.findById(BridgeBoard::class.java, boardId)
        boardNoEt.text.append(board.boardNumber.toString())

        mySeatHelper!!.setSelectedSeat(board.mySeat)
        setMySeatControlsVisible(false)

        declarerSeatHelper!!.setSelectedSeat(board.declarer)

        val hcpEt: EditText = findViewById(R.id.et_ns_hcp)
        hcpEt.text.clear()
        hcpEt.text.append(board.nsHighCardPoints.toString())

        var rg: RadioGroup = findViewById(R.id.rg_sl_fit_len)
        rg.check(when(board.scoringLineFitLength) {
            0 -> R.id.rb_sl_fit_none
            8 -> R.id.rb_sl_fit_8
            9 -> R.id.rb_sl_fit_9
            10 -> R.id.rb_sl_fit_10
            11 -> R.id.rb_sl_fit_11
            12 -> R.id.rb_sl_fit_12
            13 -> R.id.rb_sl_fit_13
            else -> throw IllegalStateException()
        })

        rg = findViewById(R.id.rg_sl_fit_suit)
        rg.check(when(board.scoringLineFitSuit) {
            Suit.clubs -> R.id.rb_sl_fit_clubs
            Suit.diamonds -> R.id.rb_sl_fit_diamonds
            Suit.hearts -> R.id.rb_sl_fit_hearts
            Suit.spades -> R.id.rb_sl_fit_spades
            Suit.none -> 0
            Suit.noTrumps -> throw IllegalStateException()
        })

        val noteEdit: EditText = findViewById(R.id.et_board_notes)
        noteEdit.setText(board.notes)

        if(board.level == 0) {
            return
        }

        rg = findViewById(R.id.rg_level)
        rg.check(when(board.level) {
            1 -> R.id.rb_level1
            2 -> R.id.rb_level2
            3 -> R.id.rb_level3
            4 -> R.id.rb_level4
            5 -> R.id.rb_level5
            6 -> R.id.rb_level6
            7 -> R.id.rb_level7
            else -> throw IllegalStateException()
        })

        rg = findViewById(R.id.rg_suit)
        rg.check(when(board.suit) {
            Suit.clubs -> R.id.rb_clubs
            Suit.diamonds -> R.id.rb_diamonds
            Suit.hearts -> R.id.rb_hearts
            Suit.spades -> R.id.rb_spades
            Suit.noTrumps -> R.id.rb_nt
            Suit.none -> throw IllegalStateException()
        })

        rg = findViewById(R.id.rg_doubled)
        rg.check(when(board.doubled) {
            Doubled.undoubled -> R.id.rb_not_doubled
            Doubled.doubled -> R.id.rb_doubled
            Doubled.redoubled -> R.id.rb_redoubled
        })

        rg = findViewById(R.id.rg_made)
        val made = board.relativeTricks() >= 0
        rg.check(if(made) R.id.rb_made else R.id.rb_down)

        tricksSpinner!!.setSelection(if(made) tricksList.indexOf(board.tricks - 6)
                                     else tricksList.indexOf(board.relativeTricks()))

        honorsSpinner!!.setSelection(Honors.values().indexOf(board.honors))

        updateVulnerableView()
        updateDealerView()
    }

    // Returns null if the entry is empty, -1 if the number is invalid
    private fun getBoardNumber(): Int? {
        val text = (findViewById<EditText>(R.id.et_board)).text.toString()
        if(text.isEmpty()) return null
        try {
            val num = text.toString().toInt()
            return if(num > MAX_BOARD_NUM) -1 else num
        } catch(e: NumberFormatException) {
            return -1
        }
    }

    private fun getMySeat() = mySeatHelper!!.getSelectedSeat()

    private fun getContractLevel(): Int? {
        val levelRadioGroup: RadioGroup = findViewById(R.id.rg_level)
        when(levelRadioGroup.checkedRadioButtonId) {
            R.id.rb_level1 -> return 1
            R.id.rb_level2 -> return 2
            R.id.rb_level3 -> return 3
            R.id.rb_level4 -> return 4
            R.id.rb_level5 -> return 5
            R.id.rb_level6 -> return 6
            R.id.rb_level7 -> return 7
            else -> return null
        }
    }

    private fun getSuit(): Suit {
        val rg: RadioGroup = findViewById(R.id.rg_suit)
        when(rg.checkedRadioButtonId) {
            R.id.rb_nt -> return Suit.noTrumps
            R.id.rb_spades -> return Suit.spades
            R.id.rb_hearts -> return Suit.hearts
            R.id.rb_diamonds -> return Suit.diamonds
            R.id.rb_clubs -> return Suit.clubs
            else -> return Suit.none
        }
    }

    private fun getDoubled(): Doubled {
        val rg: RadioGroup = findViewById(R.id.rg_doubled)
        when(rg.checkedRadioButtonId) {
            R.id.rb_not_doubled -> return Doubled.undoubled
            R.id.rb_doubled -> return Doubled.doubled
            R.id.rb_redoubled -> return Doubled.redoubled
            else -> throw IllegalStateException()
        }
    }

    private fun getNotes(): String? {
        val cb: EditText = findViewById(R.id.et_board_notes)
        val notes = cb.text.toString()
        return if(notes == "") null else notes
    }

    private fun getDeclarerSeat() = declarerSeatHelper!!.getSelectedSeat()

    private fun getMadeOrDown(): MadeOrDown {
        val rg: RadioGroup = findViewById(R.id.rg_made)
        when(rg.checkedRadioButtonId) {
            R.id.rb_made -> return MadeOrDown.made
            R.id.rb_down -> return MadeOrDown.down
            else -> return MadeOrDown.invalid
        }
    }

    private fun getHonors(): Honors {
        if(contractType == ContractBridgeType.rubber) {
            return Honors.values()[honorsSpinner!!.selectedItemPosition]
        } else {
            return Honors.none
        }
    }

    private fun getTricks(): Int? {
        val relativeTricks = tricksSpinner!!.selectedItem as? Int
        if(relativeTricks == null) return null
        doAssert(relativeTricks != 0)

        if(relativeTricks > 0) {
            return relativeTricks + 6
        } else {
            val level = getContractLevel()
            if(level == null) return null
            return level + 6 + relativeTricks
        }
    }

    private fun getNSHighCardPoints(): Int? {
        val text = (findViewById<EditText>(R.id.et_ns_hcp)).text.toString()
        if(text.isEmpty()) return null
        try {
            val num = text.toString().toInt()
            return if(num > MAX_HCP || num < MIN_HCP) -1 else num
        } catch(e: NumberFormatException) {
            return -1
        }
    }

    // Returns null for no selection and 0 for no fit
    private fun getScoringLineFitLen(): Int? {
        return when(findViewById<RadioGroup>(R.id.rg_sl_fit_len).checkedRadioButtonId) {
            R.id.rb_sl_fit_none -> 0
            R.id.rb_sl_fit_8 -> 8
            R.id.rb_sl_fit_9 -> 9
            R.id.rb_sl_fit_10 -> 10
            R.id.rb_sl_fit_11 -> 11
            R.id.rb_sl_fit_12 -> 12
            R.id.rb_sl_fit_13 -> 13
            else -> null
        }
    }

    private fun getScoringLineFitSuit(): Suit {
        return when(findViewById<RadioGroup>(R.id.rg_sl_fit_suit).checkedRadioButtonId) {
            R.id.rb_sl_fit_clubs -> Suit.clubs
            R.id.rb_sl_fit_diamonds -> Suit.diamonds
            R.id.rb_sl_fit_hearts -> Suit.hearts
            R.id.rb_sl_fit_spades -> Suit.spades
            else -> Suit.none
        }
    }

    fun updateTricksSpinner() {
        val level = getContractLevel()
        val madeOrDown = getMadeOrDown()

        tricksList.clear()

        if(level != null && madeOrDown != MadeOrDown.invalid) {
            if(madeOrDown == MadeOrDown.down) {
                for(i in -1 downTo -6 - level) tricksList.add(i)
            } else {
                for(i in level .. 7) tricksList.add(i)
            }
        }

        tricksSpinnerAdapter!!.notifyDataSetChanged()
    }

    fun getScoringLinePair(): Direction? {
        val nsHcp = getNSHighCardPoints()
        var pair: Direction? = null
        if (nsHcp != null && nsHcp >= 0) {
            pair = scoringLinePair(getBoardNumber(), nsHcp)
        }
        return pair
    }

    fun updateScoringLinePair() {
        val pair = getScoringLinePair()
        val slPairTv: TextView = findViewById(R.id.tv_scoring_line_pair)
        slPairTv.text = getString(when(pair) {
            null -> R.string.dash
            Direction.ns -> R.string.north_south
            Direction.ew -> R.string.east_west
        })
    }

    fun updateParScore() {
        var parScore: Int? = null
        val pair = getScoringLinePair()
        val nsHcp = getNSHighCardPoints()
        val slFitLen = getScoringLineFitLen()
        val slFitSuit = getScoringLineFitSuit()
        val boardNo = getBoardNumber()
        val declarer = getDeclarerSeat()

        if (declarer != Seat.none && boardNo != null && boardNo > 0) {
            val vuln = duplicateContractVulnerability(boardNo, declarer)
            if (pair != null && nsHcp != null && slFitLen != null &&
                    (slFitLen == 0 || slFitSuit != Suit.none)) {
                parScore = getParScore(
                        if (pair == Direction.ns) nsHcp else MAX_HCP - nsHcp,
                        slFitLen, slFitSuit, vuln)
            }
        }

        val parScoreTv: TextView = findViewById(R.id.tv_par_score)
        parScoreTv.text = fmtString(this, R.string.par_colon_fmt,
                if (parScore != null) parScore.toString() else getString(R.string.dash))
    }

    fun updateFitSuitRg() {
        if (getScoringLineFitLen() == 0) {
            findViewById<RadioGroup>(R.id.rg_sl_fit_suit).check(0)
        }
    }

    fun warningToast(s: String) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
    }

    fun warningToast(resId: Int) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show()
    }

    fun showNoBidDialog() {
        AlertDialog.Builder(this).setMessage(R.string.no_bid_dialog_msg)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, { dialog, bid ->
                    saveAndQuit(noBid = true)
                }).show()
    }

    fun saveAndQuit(noBid: Boolean = false) {
        val board = if(boardId != null) SugarRecord.findById(BridgeBoard::class.java, boardId)
                    else BridgeBoard()

        val boardNo = getBoardNumber()
        if(boardNo == null) {
            warningToast(R.string.board_editor_no_board_num)
            return
        } else if(boardNo <= 0) {
            warningToast(R.string.board_editor_invalid_board_num)
            return
        }

        val boardWithNo = BridgeBoardSet.boardByNumber(boardSetId, boardNo)
        if(boardWithNo != null && boardWithNo.id != board.id) {
            warningToast(fmtString(this, R.string.board_editor_board_no_used_fmt, boardNo))
            return
        }

        val mySeat = getMySeat()
        if(mySeat == Seat.none) {
            warningToast(R.string.editor_no_my_seat)
            return
        }

        var nsHCP: Int? = 0
        var slFitLen: Int? = 0
        var slFitSuit = Suit.none
        if(contractType == ContractBridgeType.parScore) {
            nsHCP = getNSHighCardPoints()
            if(nsHCP == null) {
                warningToast(R.string.board_editor_no_sl_hcp)
                return
            } else if(nsHCP < 0) {
                warningToast(R.string.board_editor_invalid_sl_hcp)
                return
            }

            slFitLen = getScoringLineFitLen()
            if(slFitLen == null) {
                warningToast(R.string.board_editor_no_sl_fit_len)
                return
            }

            slFitSuit = getScoringLineFitSuit()
            if(slFitSuit == Suit.none && slFitLen > 0) {
                warningToast(R.string.board_editor_no_sl_fit_suit)
                return
            }
        }

        var declarer = Seat.none
        var level: Int? = 0
        var suit = Suit.none
        var x = Doubled.undoubled
        var tricks: Int? = 0
        var honors = Honors.none

        if(!noBid) {
            declarer = getDeclarerSeat()
            if(declarer == Seat.none) {
                warningToast(R.string.board_editor_no_decl_seat)
                return
            }
            level = getContractLevel()
            if(level == null) {
                warningToast(R.string.board_editor_no_level)
                return
            }
            suit = getSuit()
            if(suit == Suit.none) {
                warningToast(R.string.board_editor_no_suit)
                return
            }
            x = getDoubled()

            tricks = getTricks()
            if(tricks == null) {
                warningToast(R.string.board_editor_no_tricks)
                return
            }

            honors = getHonors()

        }

        board.boardNumber = boardNo
        board.mySeat = mySeat
        board.declarer = declarer
        board.level = level!!
        board.suit = suit
        board.doubled = x
        board.tricks = tricks!!
        board.boardSetId = boardSetId
        board.honors = honors
        board.nsHighCardPoints = nsHCP!!
        board.scoringLineFitLength = slFitLen!!
        board.scoringLineFitSuit = slFitSuit
        board.notes = getNotes()

        Orm.save(board)

        if (sessionId != null) {
            var session = SugarRecord.findById(BridgeSession::class.java, sessionId)
            doAssert(session != null)
            session!!.lastEditBoard = boardNo
            Orm.save(session)
        }
        finish()
    }
}
