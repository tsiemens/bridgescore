package com.tsiemens.bridgescore.ui

import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.tsiemens.bridgescore.R
import com.tsiemens.bridgescore.rubberScoreboard
import java.util.*

class RubberScoreboardFragment : BaseBoardListFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.fragment_rubber_scoreboard, container, false)

        return v
    }

    override fun onResume() {
        super.onResume()
        updateBoardsView()
    }

    override fun updateBoardsView() {
        val scores = rubberScoreboard(brdActvity.boardList)
        var wePtSum = 0
        var theyPtSum = 0

        scores.weAbove.reverse()
        val weAboveTv: TextView = view!!.findViewById(R.id.tv_we_above_pts)
        weAboveTv.text = scores.weAbove.joinToString(separator = "\n")
        for(pt in scores.weAbove) {
            wePtSum += pt
        }

        scores.theyAbove.reverse()
        val theyAboveTv: TextView = view!!.findViewById(R.id.tv_they_above_pts)
        theyAboveTv.text = scores.theyAbove.joinToString(separator = "\n")
        for(pt in scores.theyAbove) {
            theyPtSum += pt
        }

        val belowll: LinearLayout = view!!.findViewById(R.id.ll_below_line)
        belowll.removeAllViews()

        val weGameLegs = ArrayList<Int>(2)
        val theyGameLegs = ArrayList<Int>(2)

        for(belowScore in scores.below) {
            if(belowScore.we) {
                weGameLegs.add(belowScore.points)
                wePtSum += belowScore.points
            } else {
                theyGameLegs.add(belowScore.points)
                theyPtSum += belowScore.points
            }

            if(belowScore.endOfGame || belowScore.endOfRubber) {
                addBelowRow(weGameLegs, theyGameLegs)
                weGameLegs.clear()
                theyGameLegs.clear()
            }

            if(belowScore.endOfRubber) {
                addBelowLine(true)
            } else if(belowScore.endOfGame) {
                addBelowLine(false)
            }
        }

        if(weGameLegs.size > 0 || theyGameLegs.size > 0) {
            addBelowRow(weGameLegs, theyGameLegs)
        }

        view!!.findViewById<TextView>(R.id.tv_rubber_we_pts_total).text = wePtSum.toString()
        view!!.findViewById<TextView>(R.id.tv_rubber_they_pts_total).text = theyPtSum.toString()
    }

    fun addBelowRow(wePts: List<Int>, theyPts: List<Int>) {
        val belowll: LinearLayout = view!!.findViewById(R.id.ll_below_line)
        val row = getLayoutInflater().inflate(R.layout.rubber_scoreboard_below_row, null)

        val weTv: TextView = row.findViewById(R.id.tv_we_below_pts)
        val theyTv: TextView = row.findViewById(R.id.tv_they_below_pts)

        weTv.text = wePts.joinToString(separator = "\n")
        theyTv.text = theyPts.joinToString(separator = "\n")

        belowll.addView(row)
    }

    fun addBelowLine(rubber: Boolean) {
        if(rubber) {
            addSingleBelowLine(3)
        }
        addSingleBelowLine(5)
    }

    fun addSingleBelowLine(bottomMarginDp: Int) {
        val belowll: LinearLayout = view!!.findViewById(R.id.ll_below_line)

        val horizLine = View(activity)
        horizLine.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorTextDisabled))
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2)
        params.bottomMargin = dpToPx(context!!, bottomMarginDp)
        horizLine.layoutParams = params

        belowll.addView(horizLine)
    }

}