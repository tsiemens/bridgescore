package com.tsiemens.bridgescore.ui

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import com.tsiemens.bridgescore.orm.BridgeBoardSet
import com.tsiemens.bridgescore.orm.BridgeSession
import com.tsiemens.bridgescore.orm.Orm
import java.util.*

abstract class BoardListUiState {
    val boardList = ArrayList<BridgeBoard>()
    val boardsToDelete = HashSet<Long>()

    var sessionId: Long? = null
    var session: BridgeSession? = null
    var boardSet: BridgeBoardSet? = null
    val boards = HashMap<Long, BridgeBoard>()

    fun nonDeletedBoards(): List<BridgeBoard> {
        return boards.values.filter { board -> !boardsToDelete.contains(board.id) }
    }

    fun lastBoardNumber(): Int? {
        var max = 0
        for(board in nonDeletedBoards()) {
            max = if(board.boardNumber > max) board.boardNumber else max
        }
        return if(max == 0) null else max
    }

    /**
     * Get the expected next board number to be created.
     * This will only guess a board in the last round, which is the only one missing.
     * Otherwise, null is returned.
     * eg. If the last round is 2 (3 bps) and we have played boards 4 and 6, return 5
     */
    abstract fun predictNextBoardNo(): Int?

    /**
     * Get the expected next seat position to use.
     * By default, just use the session default seat.
     */
    open fun predictNextSeat(): Seat? {
        return null
    }
}

abstract class BoardListActivity : AppCompatActivity() {

    companion object {
        val BRIDGE_SESSION_ID = "session_id"
    }

    var lastSavedInstanceState: Bundle? = null

    // TODO new class of BoardListUiState with most of this shit below

    val uiState = makeUiState()

    val boardList get() = uiState.boardList
    val boardsToDelete get() = uiState.boardsToDelete

    var sessionId get() = uiState.sessionId
        set(v) { uiState.sessionId = v }
    var session get() = uiState.session
        set(v) { uiState.session = v }
    var boardSet get() = uiState.boardSet
        set(v) { uiState.boardSet = v }
    val boards get() = uiState.boards

    // TODO Until here I think.

    var isWaitingForBoardEditor = false

    abstract fun makeUiState(): BoardListUiState

    abstract fun contentLayoutId(): Int

    private fun updateSession() {
        val sessionId = intent.getLongExtra(BRIDGE_SESSION_ID, 0L)
        doAssert(sessionId != 0L)

        session = Orm.findById(BridgeSession::class.java, sessionId)
        doAssert(session != null)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lastSavedInstanceState = savedInstanceState

        setContentView(contentLayoutId())
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            startBoardEditor()
        }

        updateSession()
    }

    override fun onResume() {
        super.onResume()

        updateSession()
        boardSet = session!!.myBoardSet()
        doAssert(boardSet != null)

        queryBoards()
    }

    override fun onPause() {
        processPendingDeletes(true)
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater;
        inflater.inflate(R.menu.board_list_menu, menu);
        menu!!.findItem(R.id.debug).isVisible = TRACE_ENABLED
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item!!.itemId) {
            android.R.id.home -> finish()
            R.id.edit -> showSessionEditor()
            R.id.debug -> startActivity(Intent(this, DebugActivity::class.java))
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun showSessionEditor() {
        val sessionDialog = EditSessionDialogFragment(session, boardSet)
        sessionDialog.onSaveButtonAction = { f -> updateSessionDetails(f) }
        sessionDialog.show(fragmentManager, "edit_session")
    }

    abstract fun updateAllViews()

    abstract fun boardIsVulnerable(board: BridgeBoard): Boolean

    abstract fun boardDisplayScore(board: BridgeBoard): Int?

    private fun updateSessionDetails(sessFrag: EditSessionDialogFragment): Boolean {
        val originalSeat = session!!.mySeatDefault
        if(sessFrag.writeToSession(session!!, boards.values)) {
            Orm.doInTransaction { ->
                Orm.save(boardSet!!)
                Orm.save(session!!)
                if(originalSeat != session!!.mySeatDefault) {
                    for (board in boards.values) {
                        Orm.save(board)
                    }
                }
            }
            updateAllViews()
            return true
        } else {
            return false
        }
    }

    fun processPendingDeletes(delete: Boolean) {
        if(boardsToDelete.isEmpty()) return
        if(!delete) {
            boardsToDelete.clear()
            updateAllViews()
        } else {
            for(id in boardsToDelete) {
                Orm.delete(boards[id]!!)
                boards.remove(id)
            }
            boardsToDelete.clear()
        }
    }

    fun queryBoards() {
        boards.clear()
        boards.putAll( boardSet!!.boards().map { board -> Pair( board.id!!, board ) } )
    }

    /**
     * Handles deletions of boards from fragments to notify that the boardsList has been modified
     * */
    abstract fun notifyBoardsListChanged()

    fun sortBoardList() {
        Collections.sort(boardList, { s1, s2 -> s1.boardNumber.compareTo(s2.boardNumber) })
    }

    abstract fun getContractType(): ContractBridgeType

    fun startBoardEditor(boardId: Long? = null) {
        val intent = Intent(this, BoardEditActivity::class.java)
        intent.putExtra(BoardEditActivity.BOARD_SET_ID, boardSet!!.id)
        if(boardId != null) {
            intent.putExtra(BoardEditActivity.BOARD_ID, boardId)
        } else {
            val boardNo: Int? = uiState.predictNextBoardNo()
            if(boardNo != null) {
                intent.putExtra(BoardEditActivity.DEFAULT_BOARD_NUMBER, boardNo)
            }

            // Go to latest round when adding a new board
            isWaitingForBoardEditor = true
        }
        intent.putExtra(BoardEditActivity.CONTRACT_TYPE, getContractType())
        val boardSeat = uiState.predictNextSeat() ?: session!!.mySeatDefault
        intent.putExtra(BoardEditActivity.MY_SEAT_KEY, boardSeat.toString())
        intent.putExtra(BoardEditActivity.SESSION_ID, session!!.id)
        startActivity(intent)
    }
}
