package com.tsiemens.bridgescore.ui

import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.tsiemens.bridgescore.R
import com.tsiemens.bridgescore.api.ApiSession
import com.tsiemens.bridgescore.api.delSession
import com.tsiemens.bridgescore.api.getSessions
import com.tsiemens.bridgescore.api.newSession
import com.tsiemens.bridgescore.compareLong
import com.tsiemens.bridgescore.orm.BridgeSession
import com.tsiemens.bridgescore.preference.PREF_DEFAULT_SESSION_NAME
import java.util.*

class SessionSelectionFragment() : SyncConfigFragment() {

    val sessionList = ArrayList<ApiSession>()
    val sessListAdapter = SessionListAdapter(sessionList)

    val session: BridgeSession
        get() = activity!!.session!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.fragment_sync_config_list, container, false)
        val fab = v.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { v -> onAddSessionFabClick() }

        val recycler = v.findViewById<RecyclerView>(R.id.list)
        recycler.setHasFixedSize(true)
        val layoutMgr = LinearLayoutManager(activity as Context)
        recycler.layoutManager = layoutMgr

        sessListAdapter.onItemClick = { v, p ->
            val oldSelected = selectedSessionIdx()
            session.syncGuid = sessionList[p].id
            session.name = sessionList[p].name
            sessListAdapter.notifyItemChanged(p)
            if(oldSelected != null) {
                sessListAdapter.notifyItemChanged(oldSelected)
            }
        }

        sessListAdapter.onItemLongClick = { v, p ->
            AlertDialog.Builder(activity!! as Context).setTitle(R.string.delete_confirm_remote_session_dialog_title)
                    .setMessage(fmtString(activity!! as Context,
                            R.string.delete_confirm_remote_session_dialog_text_fmt,
                            sessionDateString(activity!! as Context, sessionList[p].timestamp),
                            sessionList[p].name))
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, { dialog, which ->
                    DelSessionTask().execute(sessionList[p].id)
                }).show()
        }
        recycler.adapter = sessListAdapter
        return v
    }

    override fun onStart() {
        super.onStart()
        updateSessionList()
    }

    override fun onNextPressed(): Class<out SyncConfigFragment>? {
        if(session.syncGuid != null) {
            activity!!.notifyDoneConfig()
        } else {
            Toast.makeText(activity as Context, R.string.session_sync_session_required,
                    Toast.LENGTH_SHORT).show()
        }
        return null
    }

    override fun getTitle() = R.string.sync_frag_title_session
    override fun nextButtonTextId() = R.string.done

    fun onAddSessionFabClick() {
        val dialog = NewRemoteSessionDialogFragment(activity!!.session)
        dialog.onDoneAction = { sess, saveDefault ->
            if(saveDefault) {
                val prefs = PreferenceManager.getDefaultSharedPreferences(activity as Context)
                prefs.edit()
                        .putString(PREF_DEFAULT_SESSION_NAME, sess.name)
                        .apply()
            }
            NewSessionTask().execute(sess)
        }
        dialog.show(activity!!.fragmentManager, "new_session_dialog")
    }

    fun selectedSessionIdx(): Int? {
        var idx = 0
        for(sess in sessionList) {
            if(sess.id == session.syncGuid) {
                return idx
            }
            idx++
        }
        return null
    }

    fun updateSessionList() {
        RequestSessionsTask().execute()
    }

    fun sortSessionList() {
        Collections.sort(sessionList, Comparator<com.tsiemens.bridgescore.api.ApiSession> {
            lhs, rhs ->
            val comp = compareLong(rhs.timestamp, lhs.timestamp)
            if(comp != 0) {
                return@Comparator comp
            }
            compareLong(rhs.id, lhs.id)
        })
    }

    class ViewHolder(val adapter: SessionListAdapter, iv: View) : RecyclerView.ViewHolder(iv),
            View.OnClickListener, View.OnLongClickListener {
        override fun onLongClick(v: View?): Boolean {
            adapter.onItemLongClick!!(v, adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            adapter.onItemClick!!(v, adapterPosition)
        }
    }

    inner class SessionListAdapter(list: List<ApiSession>) :
            ListAdapter<ApiSession, ViewHolder>(list) {
        var onItemClick: ((v: View?, index: Int) -> Unit)? = null
        var onItemLongClick: ((v: View?, index: Int) -> Unit)? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent!!.context)
                    .inflate(R.layout.remote_session_list_item, null)
            return ViewHolder(this, view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val sess = list[position]

            val sessNameTv = getTextView(holder, R.id.tv_sess_name)
            sessNameTv.text = if(sess.name.isNotEmpty()) sess.name else "(????)"
            sessNameTv.setTextColor(
                ContextCompat.getColor(
                    context!!,
                    if(sess.id == session.syncGuid) R.color.colorAccent
                    else R.color.secondaryAccent))

            getTextView(holder, R.id.tv_date).text =
                    sessionDateString(activity!!, sess.timestamp)

            holder!!.itemView.findViewById<View>(R.id.clickable_item_bg).setOnClickListener(holder)
            holder.itemView.findViewById<View>(R.id.clickable_item_bg)
                    .setOnLongClickListener(holder)
        }
    }

    inner class RequestSessionsTask : AsyncTask<Void, Void, List<ApiSession>?>() {
        var progress: ProgressDialog? = null
        override fun onPreExecute() {
            progress = showThemedProgressDialog(activity!!, null, fmtString(activity!!,
                            R.string.loading_remote_sessions_dialog_text_fmt, session.syncServer),
                    true)
        }

        override fun doInBackground(vararg params: Void?): List<ApiSession>? {
            return getSessions(session.syncServer!!)
        }

        override fun onPostExecute(result: List<ApiSession>?) {
            progress!!.dismiss()
            progress = null

            if(result != null) {
                sessionList.clear()
                sessionList.addAll(result)
                sortSessionList()
                sessListAdapter.notifyDataSetChanged()
            } else {
                Toast.makeText(activity, fmtString(activity!!,
                        R.string.failed_to_load_remote_sessions_fmt, session.syncServer),
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class NewSessionTask : AsyncTask<ApiSession, Void, Long?>() {
        var apiSess: ApiSession? = null
        var progress: ProgressDialog? = null

        override fun onPreExecute() {
            progress = showThemedProgressDialog(activity!!, null,
                    getString(R.string.create_remote_session_dialog_text), true)
        }

        override fun doInBackground(vararg params: ApiSession): Long? {
            apiSess = params[0]
            return newSession(session.syncServer!!, session.syncServerPassword, apiSess!!)
        }

        override fun onPostExecute(newSessId: Long?) {
            progress!!.dismiss()
            progress = null
            if(newSessId != null) {
                apiSess!!.id = newSessId
                session.syncGuid = newSessId
                session.name = apiSess!!.name
                sessionList.add(apiSess!!)
                activity!!.onNextButtonPressed()
            } else {
                Toast.makeText(activity, R.string.failed_to_create_remote_session,
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class DelSessionTask : AsyncTask<Long, Void, Boolean>() {
        var sessId: Long? = null
        var progress: ProgressDialog? = null


        override fun onPreExecute() {
            progress = showThemedProgressDialog(activity!!, null,
                    getString(R.string.delete_remote_session_dialog_text), true)
        }

        override fun doInBackground(vararg params: Long?): Boolean? {
            sessId = params[0]!!
            return delSession(session.syncServer!!, session.syncServerPassword, sessId!!)
        }

        override fun onPostExecute(result: Boolean?) {
            progress!!.dismiss()
            progress = null
            if(result!!) {
                if(session.syncGuid == sessId) {
                    session.syncGuid  = null
                    session.name = ""
                }
                for(i in 0 .. sessionList.size - 1) {
                    if(sessionList[i].id == sessId!!) {
                        sessionList.removeAt(i)
                        sortSessionList()
                        sessListAdapter.notifyItemRemoved(i)
                        break
                    }
                }
            } else {
                Toast.makeText(activity, R.string.failed_to_delete_remote_session,
                        Toast.LENGTH_SHORT).show()
            }
        }
    }
}