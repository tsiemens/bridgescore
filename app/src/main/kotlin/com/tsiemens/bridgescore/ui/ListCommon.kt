package com.tsiemens.bridgescore.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import com.tsiemens.bridgescore.orm.OrmRecord


class CircleCheckViewHolder<T : OrmRecord>(val adapter: CircleCheckListAdapter<T>,
                                           itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

    override fun onClick(v: View?) {
        if(v!!.id == R.id.rl_circle_container) {
            adapter.toggleCircleSelectorChecked(itemView, adapterPosition)
            adapter.onItemSelectorClick!!(itemView, adapterPosition)
        } else {
            adapter.onItemClick!!(itemView, adapterPosition)
        }
    }
}

fun getTextView(holder: RecyclerView.ViewHolder?, viewId: Int): TextView {
    return holder!!.itemView.findViewById(viewId)
}

abstract class ListAdapter<T, VH: RecyclerView.ViewHolder>(val list: List<T>) :
        RecyclerView.Adapter<VH>() {

    override fun getItemCount(): Int = list.size
}

abstract class CircleCheckListAdapter<T : OrmRecord>(
        list: List<T>, val selections: Set<Long>,
        val itemLayoutResId: Int) :
        ListAdapter<T, CircleCheckViewHolder<T>>(list) {

    var onItemClick: ((v: View?, index: Int) -> Unit)? = null
    var onItemSelectorClick: ((v: View?, index: Int) -> Unit)? = null

    var circleDataViewId: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CircleCheckViewHolder<T> {
        val view = LayoutInflater.from(parent!!.context).inflate(itemLayoutResId, null)
        return CircleCheckViewHolder(this, view)
    }

    fun onBindViewHolderCommon(holder: CircleCheckViewHolder<T>, item: T) {
        setCircleSelectorChecked(holder.itemView, selections.contains(item.getid()))

        holder.itemView.findViewById<View>(R.id.rl_clickable_item_bg).setOnClickListener(holder)
        holder.itemView.findViewById<View>(R.id.rl_circle_container).setOnClickListener(holder)
    }

    fun toggleCircleSelectorChecked(container: View, itemIdx: Int) {
        setCircleSelectorChecked(container, !selections.contains(list[itemIdx].getid()))
    }

    fun setCircleSelectorChecked(container: View, selected: Boolean) {
        val imgView: ImageView = container.findViewById(R.id.iv_select_circle)
        val numView: View? = if(circleDataViewId != null)
            container.findViewById(circleDataViewId!!) else null
        if (selected) {
            imgView.setImageResource(R.drawable.circle_selected)
            if(numView != null) {
                numView.visibility = View.INVISIBLE
            }
        } else {
            imgView.setImageResource(R.drawable.circle_unselected)
            if(numView != null) {
                numView.visibility = View.VISIBLE
            }
        }
    }
}

fun bindBoardContractViewHolder(holder: RecyclerView.ViewHolder, board: BridgeBoard,
                                madeText: String, score: Int?, vuln: Boolean,
                                pointsFmt: String = "%s") {
    val suitIv: ImageView = holder.itemView.findViewById(R.id.iv_suit)
    val levelTv = getTextView(holder, R.id.tv_level)
    val tvTricks = getTextView(holder, R.id.tv_tricks)
    val tvDoubled = getTextView(holder, R.id.tv_doubled)
    val scoreTv = getTextView(holder, R.id.tv_score)

    if(board.level == 0) {
        // No bid
        levelTv.text = holder.itemView.context.getString(R.string.no_bid)
        suitIv.visibility = View.GONE
        tvTricks.text = ""
        tvDoubled.visibility = View.GONE
    } else {
        levelTv.text = board.level.toString()
        suitIv.visibility = View.VISIBLE
        suitIv.setImageResource(suitImageResId(board.suit))

        val relTricks = board.relativeTricks()
        tvTricks.text = when {
            relTricks > 0 -> "+" + relTricks.toString()
            relTricks < 0 -> relTricks.toString()
            else -> madeText
        }

        tvDoubled.text = when(board.doubled) {
            Doubled.undoubled -> ""
            Doubled.doubled -> "X"
            Doubled.redoubled -> "XX"
        }
        tvDoubled.visibility = if(board.doubled == Doubled.undoubled) View.GONE else View.VISIBLE
    }

    getTextView(holder, R.id.tv_vul).visibility = if(vuln) View.VISIBLE else View.GONE

    if(score != null) {
        setBoardPointsText(scoreTv, score, pointsFmt)
    } else {
        scoreTv.text = ""
    }

    val honorsTv: TextView? = holder.itemView!!.findViewById(R.id.tv_honors)
    if(honorsTv != null) {
        honorsTv.text = if(board.honors != Honors.none) fmtString(honorsTv.context,
                R.string.contract_honors_fmt, honorsTv.context.getString(board.honors.strRes))
                else ""
    }
}
