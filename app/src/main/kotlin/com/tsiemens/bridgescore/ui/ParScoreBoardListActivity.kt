package com.tsiemens.bridgescore.ui

import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard

class ParScoreBoardListUiState : BoardListUiState() {
    /**
     * Get the expected next board number to be created.
     * In par-score duplicate, we can assume it is just the last board + 1
     */
    override fun predictNextBoardNo(): Int? {
        val lastBoard = lastBoardNumber()
        if(lastBoard != null) {
            return lastBoard + 1
        } else {
            return 1
        }
    }
}

class ParScoreBoardListActivity : BoardListActivity() {

    override fun makeUiState(): BoardListUiState {
        return ParScoreBoardListUiState()
    }

    override fun contentLayoutId() = R.layout.activity_parscore_board_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val listFrag = BoardListFragment()
        supportFragmentManager.beginTransaction().add(R.id.frag_container, listFrag)
                .setTransition(FragmentTransaction.TRANSIT_NONE)
                .commit()
    }

    override fun onResume() {
        super.onResume()
        updateAllViews()
        isWaitingForBoardEditor = false
    }

    override fun updateAllViews() {
        updateBoardList()
        updateIMPTotals()
    }

    fun getCurrentFragment(): BaseBoardListFragment {
        return supportFragmentManager.findFragmentById(R.id.frag_container) as BaseBoardListFragment
    }

    fun updateIMPTotals() {
        var myImps = 0
        var theirImps = 0
        for (board in uiState.nonDeletedBoards()) {
            val imps = parScoreBoardIMPs(board)
            if (imps > 0) {
                myImps += imps
            } else {
                theirImps -= imps
            }
        }
        (findViewById<TextView>(R.id.tv_we_imps)).text =
                fmtString(this, R.string.we_imps_total_fmt, myImps)
        (findViewById<TextView>(R.id.tv_they_imps)).text =
                fmtString(this, R.string.they_imps_total_fmt, theirImps)
    }

    fun updateBoardList() {
        boardList.clear()
        boardList.addAll(uiState.nonDeletedBoards())
        sortBoardList()
        getCurrentFragment().updateBoardsView()
    }

    override fun boardIsVulnerable(board: BridgeBoard): Boolean {
        return duplicateContractVulnerability(board)
    }

    override fun boardDisplayScore(board: BridgeBoard): Int? {
        val dupScore = duplicateBoardScore(board)
        // Show the duplicate score as the actual contract score. IMPs will be relative
        // to the user.
        return if(board.declarer.direction() == board.mySeat.direction()) dupScore
               else -dupScore
    }

    override fun getContractType(): ContractBridgeType = ContractBridgeType.parScore

    /**
     * Handles deletions of boards from fragments to notify that the boardsList has been modified
     * */
    override fun notifyBoardsListChanged() {
        getCurrentFragment().updateBoardsView()
        updateIMPTotals()
    }
}
