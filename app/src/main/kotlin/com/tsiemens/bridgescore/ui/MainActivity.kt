package com.tsiemens.bridgescore.ui

import android.app.Dialog
import android.app.DialogFragment
import android.content.Intent
import android.os.Bundle
import android.util.LongSparseArray
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.orm.SugarContext
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeSession
import com.tsiemens.bridgescore.orm.BridgeBoardSet
import com.tsiemens.bridgescore.orm.Orm
import java.util.*

class MainActivity : AppCompatActivity() {

    val selectedSessions = HashSet<Long>()
    var sessionsToDelete = HashSet<BridgeSession>()

    val sessionList = ArrayList<BridgeSession>()
    val myBoardsBySessionId = LongSparseArray<BridgeBoardSet>()
    val adapter = SessionListAdapter(sessionList, selectedSessions)

    var actionMode: ActionMode? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main);
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            val sessionDialog = EditSessionDialogFragment()
            sessionDialog.onSaveButtonAction = { f -> createNewBridgeSession(f) }
            sessionDialog.show(fragmentManager, "new_session")
        }

        SugarContext.init(this)

        val recycler: RecyclerView = findViewById(R.id.list)
        recycler.setHasFixedSize(true)

        val layoutMgr = LinearLayoutManager(this)
        recycler.layoutManager = layoutMgr

        adapter.circleDataViewId = R.id.tv_session_type
        adapter.onItemClick = { v, i ->
            val sess = sessionList[i]
            startSessionActivity( sess.id!!, sess.type )
        }
        adapter.onItemSelectorClick = { v, i ->
            if(selectedSessions.contains(sessionList[i].id)) {
                selectedSessions.remove(sessionList[i].id)
                if(selectedSessions.isEmpty()) {
                    actionMode!!.finish()
                }
            } else {
                selectedSessions.add(sessionList[i].id!!)
                if(actionMode == null) {
                    actionMode = startSupportActionMode(SessionsActionModeCb(this))
                }
            }
        }
        recycler.adapter = adapter

        Trace.d(funcTag(), "Starting with device Id: " + getDeviceId(this))
    }

    inner class SessionsActionModeCb(val activity: MainActivity) : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            MenuInflater(activity).inflate(R.menu.delete_action_menu, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
            if(item!!.itemId == R.id.delete) {
                // Clear previous delete queue
                processPendingDeletes(true)

                for(session in sessionList) {
                    if(selectedSessions.contains(session.id)) {
                        sessionsToDelete.add(session)
                    }
                }

                sessionList.removeAll(sessionsToDelete)
                adapter.notifyDataSetChanged()

                val fab: View = activity.findViewById(R.id.fab)

                Snackbar.make(fab, fmtString(this@MainActivity,
                        R.string.session_list_deleted_sessions_fmt, sessionsToDelete.size), 5000)
                        .setCallback(object: Snackbar.Callback() {
                            override fun onDismissed(snackbar: Snackbar?, event: Int) {
                                processPendingDeletes(event !=
                                        Snackbar.Callback.DISMISS_EVENT_ACTION)
                            }
                        })
                        .setAction(R.string.undo, { v -> }).show()
                activity.actionMode!!.finish()
                return true
            }
            return false
        }

        override fun onDestroyActionMode(mode: ActionMode?) {
            activity.actionMode = null
            activity.adapter.notifyDataSetChanged()
            activity.selectedSessions.clear()
        }
    }

    fun processPendingDeletes(delete: Boolean) {
        if(sessionsToDelete.isEmpty()) return
        if(!delete) {
            sessionList.addAll(sessionsToDelete)
        } else {
            for(session in sessionsToDelete) {
                myBoardsBySessionId.remove(session.id!!)
                Orm.deleteSessionAndBoards(session)
            }
        }
        sessionsToDelete.clear()
        sortSessionList()
        adapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        updateSessionList()
    }

    override fun onPause() {
        processPendingDeletes(true)
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater;
        inflater.inflate(R.menu.main_activity_menu, menu);
        menu!!.findItem(R.id.debug).isVisible = TRACE_ENABLED
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item!!.itemId) {
            R.id.settings -> startSettingsActivity()
            R.id.about -> showAboutDialog()
            R.id.debug -> startActivity(Intent(this, DebugActivity::class.java))
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    fun updateSessionList() {
        sessionList.clear()
        myBoardsBySessionId.clear()
        for( s in Orm.findAll(BridgeSession::class.java) ) {
            sessionList.add(s)
            myBoardsBySessionId.append(s.id!!, s.myBoardSet())
        }
        sortSessionList()
        adapter.notifyDataSetChanged()
    }

    fun sortSessionList() {
        Collections.sort(sessionList, { s1, s2 -> s2.id!!.compareTo(s1.id!!) })
    }

    fun startSessionActivity(sessionId: Long, type: ContractBridgeType) {
        val clazz = when(type) {
            ContractBridgeType.invalid -> throw IllegalArgumentException()
            ContractBridgeType.duplicate, ContractBridgeType.teams ->
                DuplicateBoardListActivity::class.java
            ContractBridgeType.rubber -> RubberBoardListActivity::class.java
            ContractBridgeType.parScore -> ParScoreBoardListActivity::class.java
        }
        val intent = Intent(this, clazz)
        intent.putExtra(BoardListActivity.BRIDGE_SESSION_ID, sessionId)
        startActivity(intent)
    }

    fun createNewBridgeSession(sessFrag: EditSessionDialogFragment): Boolean {
        val session = BridgeSession(timestamp = System.currentTimeMillis())
        if(sessFrag.writeToSession(session)) {
            Orm.doInTransaction { ->
                Orm.save(session)
                val myBoards = BridgeBoardSet()
                myBoards.sessionId = session.id!!
                Orm.save(myBoards)
                startSessionActivity(session.id!!, session.type)
            }
            return true
        } else {
            return false
        }
    }

    inner class SessionListAdapter(list: List<BridgeSession>, selections: Set<Long>) :
            CircleCheckListAdapter<BridgeSession>(list, selections, R.layout.session_list_item) {

        override fun onBindViewHolder(holder: CircleCheckViewHolder<BridgeSession>, position: Int) {
            val session = list[position]
            val ctx = holder!!.itemView.context

            val bufferView = holder.itemView.findViewById<View>(R.id.bottom_buffer_view)
            bufferView.visibility = if(position == list.size - 1) View.VISIBLE else View.GONE


            getTextView(holder, R.id.tv_session_type).text =  ctx.getString(when(session.type) {
                ContractBridgeType.invalid -> throw IllegalStateException()
                ContractBridgeType.duplicate -> R.string.session_type_char_duplicate
                ContractBridgeType.teams -> R.string.session_type_char_teams
                ContractBridgeType.rubber -> R.string.session_type_char_rubber
                ContractBridgeType.parScore -> R.string.session_type_char_par_score
            })

            val count = myBoardsBySessionId.get(session.id!!).boardCount()
            getTextView(holder, R.id.tv_board_count).text =
                    fmtString(ctx, if(count == 1L) R.string.session_board_count_fmt
                    else R.string.session_board_count_plural_fmt, count)

            getTextView(holder, R.id.tv_date).text =
                    sessionDateString(ctx, session.timestamp)
            getTextView(holder, R.id.tv_team).text = myBoardsBySessionId.get(session.id!!).teamName
            getTextView(holder, R.id.tv_sess_name).text = session.name

            onBindViewHolderCommon(holder, session)
        }
    }

    fun startSettingsActivity() {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    fun showAboutDialog() {
        AboutDialogFragment().show(fragmentManager, "about")
    }

    class AboutDialogFragment() : DialogFragment() {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {
            val packageInfo = activity.packageManager.getPackageInfo(activity.packageName, 0)

            val builder = AlertDialog.Builder(activity);
            builder.setTitle(fmtString(activity, R.string.about_title_fmt,
                    resources.getString(R.string.app_name)))
                    .setMessage(fmtString(activity, R.string.about_version_copy_fmt,
                            packageInfo.versionName, packageInfo.versionCode,
                            resources.getString(R.string.copyright)))
                    .setPositiveButton(android.R.string.ok, null)

            return builder.create();
        }
    }
}
