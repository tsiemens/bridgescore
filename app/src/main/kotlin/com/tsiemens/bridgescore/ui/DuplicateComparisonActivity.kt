package com.tsiemens.bridgescore.ui

import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.LongSparseArray
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.orm.dsl.BuildConfig
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.api.ApiBoardSet
import com.tsiemens.bridgescore.api.getBoardsSets
import com.tsiemens.bridgescore.api.updateBoardSet
import com.tsiemens.bridgescore.orm.*

import java.util.*

class DuplicateComparisonActivity : AppCompatActivity() {

    companion object {
        val BRIDGE_SESSION_ID = "session_id"

        private val SAVED_SPINNER_SELECTION = "spinner_idx"
    }

    var sessionId: Long = -1
    var session: BridgeSession? = null
    val boardSetsById = LongSparseArray<BridgeBoardSet>()
    var myBoardSetId: Long? = null

    // Used just for recycler data
    val boardSets = ArrayList<BridgeBoardSet>()
    val boardSetAdapter = TeamListAdapter(boardSets)
    val roundBoards = ArrayList<BridgeBoard>()
    val roundBoardsAdapter = BoardListAdapter(roundBoards)

    val boardsBySetId = HashMap<Long, Map<Int, BridgeBoard>>()
    var boardAggregator: BoardPointsAggregator? = null

    var boardSpinner: Spinner? = null
    val boardsSpinnerList = ArrayList<String>()
    var boardSpinnerAdapter: ArrayAdapter<String>? = null

    var recycler: RecyclerView? = null

    var updatingSpinner = false

    var hasResumedPreviously = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_duplicate_comparison)
        val toolbar: Toolbar? = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        sessionId = intent.getLongExtra("session_id", -1)
        doAssert(sessionId != -1L)

        session = Orm.findById(BridgeSession::class.java, sessionId)
        when(session!!.type) {
            ContractBridgeType.duplicate ->
                boardAggregator = DuplicateBoardAggregator(boardsBySetId)
            ContractBridgeType.teams ->
                boardAggregator = TeamsBoardAggregator(boardsBySetId)
            ContractBridgeType.rubber,
            ContractBridgeType.parScore,
            ContractBridgeType.invalid ->
                throw IllegalStateException()
        }

        boardSpinner = findViewById(R.id.spinner_board)
        boardSpinnerAdapter = ArrayAdapter(this, R.layout.appbar_spinner_item, boardsSpinnerList)
        boardSpinnerAdapter!!.setDropDownViewResource(R.layout.simple_spinner_item)
        boardSpinner!!.adapter = boardSpinnerAdapter
        boardSpinner!!.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(!updatingSpinner) {
                    updateRecyclerContents()
                }
            }
        }

        recycler = findViewById(R.id.list)
        recycler!!.setHasFixedSize(true)
        val layoutMgr = LinearLayoutManager(this)
        recycler!!.layoutManager = layoutMgr
        recycler!!.adapter = boardSetAdapter
    }

    override fun onResume() {
        super.onResume()
        val isFirstResume = !hasResumedPreviously
        hasResumedPreviously = true
        if (!isFirstResume) {
            // Before the first resume, this is done in onCreate
            session = Orm.findById(BridgeSession::class.java, sessionId)
        } else {
            doAssert(session != null)
        }
        invalidateOptionsMenu()
        if(session!!.syncMode == SessionSyncMode.noSync) {
            if (isFirstResume) {
                startSyncSettings()
            } else {
                finish()
            }
            return
        }
        doAssert(session!!.syncGuid != null)
        doAssert(session!!.syncServer != null)
        updateBoardSets()
        PullBoardsTask().execute()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.duplicate_comparison_menu, menu)

        menu!!.findItem(R.id.generate).isVisible = BuildConfig.DEBUG
        menu.findItem(R.id.debug).isVisible = TRACE_ENABLED
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        super.onPrepareOptionsMenu(menu)
        menu!!.findItem(R.id.publish).isVisible = session!!.syncMode == SessionSyncMode.pushPull
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item!!.itemId) {
            android.R.id.home -> finish()
            R.id.publish -> PushBoardsTask().execute()
            R.id.sync -> PullBoardsTask().execute()
            R.id.sync_settings -> startSyncSettings()
            R.id.generate -> testGenerateOpponents()
            R.id.debug -> startActivity(Intent(this, DebugActivity::class.java))
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    fun updateBoardsSpinner() {
        updatingSpinner = true
        boardsSpinnerList.clear()
        boardsSpinnerList.add(getString(R.string.total))
        for(n in boardAggregator!!.boardNumbers) {
            boardsSpinnerList.add(fmtString(this, R.string.board_number_fmt, n))
        }
        boardSpinnerAdapter!!.notifyDataSetChanged()
        updatingSpinner = false
    }

    enum class RecyclerItemType {
        invalid,
        boardSet,
        board,
    }

    fun recyclerItemType(): RecyclerItemType {
        val selection = boardSpinner!!.selectedItemPosition
        return when {
            selection == 0 -> RecyclerItemType.boardSet
            selection > 0 -> RecyclerItemType.board
            else -> RecyclerItemType.invalid
        }
    }

    fun updateRecyclerContents() {
        when (recyclerItemType()) {
            RecyclerItemType.boardSet -> {
                recycler!!.adapter = boardSetAdapter;
                boardSetAdapter.notifyDataSetChanged();
            }
            RecyclerItemType.board -> {
                val boardNo = boardSpinner!!.selectedItemPosition - 1
                val boardNumber = boardAggregator!!.boardNumbers[boardNo]
                roundBoards.clear()
                for (set in boardSets) {
                    val board = set.boardWithNumber(boardNumber)
                    if (board != null) {
                        roundBoards.add(board)
                    }
                }

                roundBoards.sortByDescending { b ->
                    val sortVal: Float
                    val imp = boardAggregator!!.boardIMPs(b.id!!)
                    if (imp != null) {
                        sortVal = imp.toFloat()
                    } else {
                        if (boardAggregator!!.matchpointsSupported()) {
                            sortVal = boardAggregator!!.boardMatchpointPct(b) - 10000
                        } else {
                            sortVal = b.id!!.toFloat() - 10000000
                        }
                    }
                    sortVal
                }
                recycler!!.adapter = roundBoardsAdapter
                roundBoardsAdapter.notifyDataSetChanged()
            }
            RecyclerItemType.invalid -> {}
        }
        // Otherwise, this should react when the spinner selects
    }

    fun updateBoardSets() {
        boardSets.clear()
        boardSets.addAll(session!!.boardSets())
        boardSetsById.clear()
        boardsBySetId.clear()
        var mySetIdx: Int? = null
        var idx = 0
        for(set in boardSets) {
            boardSetsById.put(set.id!!, set)
            if(set.isMine()) {
                myBoardSetId = set.id!!
                if(boardAggregator is TeamsBoardAggregator) {
                    (boardAggregator!! as TeamsBoardAggregator).myBoardSetId = myBoardSetId
                }
                if(session!!.syncMode == SessionSyncMode.pullOnly) {
                    mySetIdx = idx
                }
            } else {
                if(set.publisherId != null && set.publisherId == session!!.teammatePublisherId) {
                    if(boardAggregator is TeamsBoardAggregator) {
                        (boardAggregator!! as TeamsBoardAggregator).teammateBoardSetId = set.id!!
                    }
                }
            }

            idx++
        }
        // In pull only mode, we don't try to compare our local copy
        if(mySetIdx != null) {
            boardSets.removeAt(mySetIdx)
        }

        for(set in boardSets) {
            val boards = HashMap<Int, BridgeBoard>()
            for(board in set.boards()) {
                boards[board.boardNumber] = board
            }
            boardsBySetId.put(set.id!!, boards)
        }

        boardAggregator!!.notifyBoardSetsChanged()

        boardSets.sortByDescending { bs ->
            val sortVal: Float
            val imp = boardAggregator!!.teamIMPs(bs.id!!)
            if (imp != null) {
                sortVal = imp.toFloat()
            } else {
                if (boardAggregator!!.matchpointsSupported()) {
                    sortVal = boardAggregator!!.teamMatchpointPct(bs.id!!) - 10000
                } else {
                    sortVal = bs.id!!.toFloat() - 10000000
                }
            }
            sortVal
        }

        updateBoardsSpinner()
        updateRecyclerContents()
    }

    inner class PushBoardsTask : AsyncTask<Void, Void, Boolean>() {
        var progress: ProgressDialog? = null

        override fun onPreExecute() {
            progress = showThemedProgressDialog(this@DuplicateComparisonActivity, null,
                    "Publishing boards to ${session!!.syncServer} ...", true)
        }

        override fun doInBackground(vararg params: Void?): Boolean {
            val myBoardSet = boardSetsById[myBoardSetId!!]
            val apiBoardSet = ApiBoardSet(myBoardSet, myBoardSet.boards())
            return updateBoardSet(session!!.syncServer!!, session!!.syncServerPassword,
                    session!!.syncGuid!!, apiBoardSet, getDeviceId(this@DuplicateComparisonActivity))
        }

        override fun onPostExecute(result: Boolean) {
            progress!!.dismiss()
            progress = null

            if(result) {
                PullBoardsTask().execute()
            } else {
                Toast.makeText(this@DuplicateComparisonActivity,
                        R.string.failed_to_publish_scores, Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class PullBoardsTask : AsyncTask<Void, Void, List<ApiBoardSet>?>() {
        var progress: ProgressDialog? = null

        override fun onPreExecute() {
            progress = showThemedProgressDialog(this@DuplicateComparisonActivity, null,
                    "Getting boards from ${session!!.syncServer} ...", true)
        }

        override fun doInBackground(vararg params: Void?): List<ApiBoardSet>? {
            return getBoardsSets(session!!.syncServer!!, session!!.syncGuid!!)
        }

        override fun onPostExecute(result: List<ApiBoardSet>?) {
            progress!!.dismiss()
            progress = null

            if(result != null) {
                val myDevIdHash = sha1HashHex(getDeviceId(this@DuplicateComparisonActivity))
                        .toLowerCase()

                var foundMyBoardPublished = false
                var showPublishWarning = false
                Orm.doInTransaction {
                    for(set in boardSets) {
                        if(!set.isMine()) {
                            Orm.deleteBoardSetAndBoards(set)
                        }
                    }
                    for(set in result) {
                        if(set.boardSet.publisherId!!.toLowerCase() == myDevIdHash &&
                                session!!.syncMode == SessionSyncMode.pushPull) {

                            foundMyBoardPublished = true
                            if (!myRemoteBoardSetIsUpToDate(set)) {
                                // If our boards remote version is not up to date, warn the user
                                showPublishWarning = true
                            }

                            // Ignore our board if we are pushing
                            continue
                        }
                        set.boardSet.sessionId = sessionId
                        Orm.save(set.boardSet)
                        for(board in set.boards) {
                            board.boardSetId = set.boardSet.id
                            Orm.save(board)
                        }
                    }
                }

                if (!foundMyBoardPublished && session!!.myBoardSet().boardCount() > 0 &&
                    session!!.syncMode == SessionSyncMode.pushPull) {
                    showPublishWarning = true
                }

                updateBoardSets()
                if (showPublishWarning) {
                    showPublishSnackBar()
                } else {
                    maybeShowTeamSelectHint()
                }
            } else {
                Toast.makeText(this@DuplicateComparisonActivity,
                        R.string.failed_to_get_scores_from_server,
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun myRemoteBoardSetIsUpToDate(remoteSet: ApiBoardSet): Boolean {
        val myBoardSet = session!!.myBoardSet()
        if (remoteSet.boardSet.teamName != myBoardSet.teamName) {
            return false
        }
        val myBoards = myBoardSet.boards().sortedBy { b -> b.boardNumber }
        val remoteBoards = remoteSet.boards.sortedBy { b -> b.boardNumber }

        if (myBoards.size != remoteBoards.size) {
            return false
        }

        return (0..myBoards.size - 1).none { !myBoards[it].boardEquals(remoteBoards[it]) }
    }

    fun showPublishSnackBar() {
        Snackbar.make(recycler!!, R.string.unpublished_changes_snackbar, Snackbar.LENGTH_LONG)
                .setAction(R.string.publish, { v ->
                    PushBoardsTask().execute()
                })
                .setCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        maybeShowTeamSelectHint()
                        super.onDismissed(snackbar, event)
                    }
                })
                .show()
    }

    fun maybeShowTeamSelectHint() {
        if (session!!.type == ContractBridgeType.teams && session!!.teammatePublisherId == null) {
            Toast.makeText(this@DuplicateComparisonActivity, R.string.select_team_hint,
                           Toast.LENGTH_LONG).show()
        }
    }

    fun testGenerateOpponents() {
        val myboards = session!!.myBoardSet().boards()
        val newBoardSet = BridgeBoardSet(teamName = "Test team", sessionId = session!!.id,
                publisherId = "abcd")
        Orm.save(newBoardSet)
        for(board in myboards) {
            val newBoard = BridgeBoard(boardNumber = board.boardNumber,
                    level = 4, suit = Suit.spades, doubled = Doubled.undoubled,
                    tricks = 10, declarer = Seat.south, mySeat = board.mySeat,
                    boardSetId = newBoardSet.id)
            Orm.save(newBoard)
        }
        updateBoardSets()
        boardSetAdapter.notifyDataSetChanged()
    }

    fun bindViewHolderCommon(holder: ViewHolder?, boardSet: BridgeBoardSet,
                             mpts: Float?, maxMpts: Int?, imps: Int?) {

        getTextView(holder, R.id.tv_team).text = boardSet.teamName
        getTextView(holder, R.id.tv_team).setTextColor( ContextCompat.getColor(this,
                when {
                    boardSet.isMine() -> R.color.colorAccent
                    boardSet.publisherId == session!!.teammatePublisherId -> R.color.secondaryAccent
                    else -> android.R.color.white
                }))

        val mpsPctStr: String?
        val mpsFractionStr: String?
        if (mpts != null) {
            mpsPctStr = String.format("%s%%", prettyRoundedFloat(matchpointPct(mpts, maxMpts!!)))
            mpsFractionStr = String.format("(%s/%d MP)", prettyRoundedFloat(mpts), maxMpts)
        } else {
            mpsPctStr = null
            mpsFractionStr = null
        }

        getTextView(holder, R.id.tv_points_primary).text = when {
            imps != null -> String.format("%d IMP", imps)
            mpsPctStr != null -> mpsPctStr
            else -> ""
        }

        if (mpsPctStr != null) {
            getTextView(holder, R.id.tv_matchpoints_sub).text =
                    if (imps != null) String.format("%s %s", mpsPctStr, mpsFractionStr)
                    else mpsFractionStr
            getTextView(holder, R.id.tv_matchpoints_sub).visibility = View.VISIBLE
        } else {
            getTextView(holder, R.id.tv_matchpoints_sub).visibility = View.GONE

        }

        val itemClickArea: View = holder!!.itemView.findViewById(R.id.clickable_item_bg)
        if (session!!.type == ContractBridgeType.teams) {
            // Long click is to set the other team component
            itemClickArea.setOnLongClickListener(holder)
            itemClickArea.isLongClickable = true
        } else {
            itemClickArea.isLongClickable = false
        }
    }

    class ViewHolder(iv: View, val adapter: ComparisonListAdapter<*>) : RecyclerView.ViewHolder(iv),
            View.OnLongClickListener {
        override fun onLongClick(v: View?): Boolean {
            adapter.onItemLongClick(v, adapterPosition)
            return true
        }
    }

    abstract inner class ComparisonListAdapter<D>(list: List<D>) :
            ListAdapter<D, ViewHolder>(list) {

        abstract fun boardSetIdForItemIndex(index: Int): Long

        fun onItemLongClick(v: View?, index: Int) {
            if (session!!.type != ContractBridgeType.teams) {
                return
            }
            val teamPublishderId: String?
            val boardSetId = boardSetIdForItemIndex(index)

            teamPublishderId = boardSetsById[boardSetId].publisherId

            if (teamPublishderId == null) {
                // The board set is ours, and we cannot be our own teammate
                Toast.makeText(this@DuplicateComparisonActivity,
                        R.string.cannot_select_self_for_team, Toast.LENGTH_SHORT).show()
            } else {
                // The teammatePublisherId will be updated
                if (teamPublishderId == session!!.teammatePublisherId) {
                    // Re-selecting the same team. This should unselect the team
                    session!!.teammatePublisherId = null
                    (boardAggregator!! as TeamsBoardAggregator).teammateBoardSetId = null
                    Toast.makeText(this@DuplicateComparisonActivity,
                            "Teammate reset", Toast.LENGTH_SHORT).show()
                } else {
                    if (boardAggregator!!.teamPositions[boardSetId] ==
                            boardAggregator!!.teamPositions[myBoardSetId!!]) {
                        Toast.makeText(this@DuplicateComparisonActivity,
                                "Warning: teammate is playing the same direction as you",
                                Toast.LENGTH_SHORT).show()
                    }
                    session!!.teammatePublisherId = teamPublishderId
                    (boardAggregator!! as TeamsBoardAggregator).teammateBoardSetId = boardSetId
                }
                Orm.save(session!!)
                updateBoardSets()
            }
        }
    }

    inner class TeamListAdapter(list: List<BridgeBoardSet>) :
            ComparisonListAdapter<BridgeBoardSet>(list) {

        override fun boardSetIdForItemIndex(index: Int): Long {
            return list[index].id!!
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent!!.context)
                    .inflate(R.layout.duplicate_comparison_list_item, null)
            return ViewHolder(view, this)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val boardSet = list[position]

            var mps: Float? = null
            var maxMps: Int? = null
            val imps: Int?
            if(boardAggregator!!.matchpointsSupported()) {
                mps = boardAggregator!!.teamMatchpoints(boardSet.id!!)
                maxMps = boardAggregator!!.maxMatchpointsForTeam(boardSet.id!!)
            }
            imps = boardAggregator!!.teamIMPs(boardSet.id!!)

            bindViewHolderCommon(holder, boardSet, mps, maxMps, imps)
            setDirectionText(getTextView(holder, R.id.tv_direction),
                    boardAggregator!!.teamPositions[boardSet.id!!] == Direction.ns)
            setBoardPointsText(getTextView(holder, R.id.tv_score),
                    boardAggregator!!.pairContractPoints(boardSet.id!!), fmt = "(%s)")

            holder!!.itemView.findViewById<View>(R.id.ll_contract).visibility = View.GONE
        }
    }

    inner class BoardListAdapter(list: List<BridgeBoard>) :
            ComparisonListAdapter<BridgeBoard>(list) {

        override fun boardSetIdForItemIndex(index: Int): Long {
            return list[index].boardSetId!!
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent!!.context)
                    .inflate(R.layout.duplicate_comparison_list_item, null)
            return ViewHolder(view, this)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val board = list[position]
            val boardSet = boardSetsById[board.boardSetId!!]

            var mps: Float? = null
            var maxMps: Int? = null
            if(boardAggregator!!.matchpointsSupported()) {
                mps = boardAggregator!!.boardMatchpoints(board.id!!)
                maxMps = boardAggregator!!.maxBoardMatchpoints(board)
            }
            val imps = boardAggregator!!.boardIMPs(board.id!!)

            bindViewHolderCommon(holder, boardSet, mps, maxMps, imps)

            setDirectionText(getTextView(holder, R.id.tv_direction), board.playerIsNorthSouth())

            getTextView(holder, R.id.tv_declarer).text = when(board.declarer) {
                Seat.north -> getString(R.string.north_abv)
                Seat.east -> getString(R.string.east_abv)
                Seat.south -> getString(R.string.south_abv)
                Seat.west -> getString(R.string.west_abv)
                Seat.none -> "???"
            }

            holder!!.itemView.findViewById<View>(R.id.ll_contract).visibility = View.VISIBLE
            bindBoardContractViewHolder(holder, board, "-",
                    duplicateBoardScore(board), duplicateContractVulnerability(board),
                    pointsFmt = "(%s)")
        }
    }

    fun startSyncSettings() {
        val intent = Intent(this, SyncConfigActivity::class.java)
        intent.putExtra(SyncConfigActivity.BRIDGE_SESSION_ID, sessionId)
        startActivity(intent)
    }

}
