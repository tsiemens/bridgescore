package com.tsiemens.bridgescore.preference

val PREF_DEFAULT_SESSION_NAME: String = "default_session_name"
val PREF_DEFAULT_TEAM_NAME: String = "default_team_name"
val PREF_DEFAULT_BOARDS_PER_ROUND: String = "default_boards_per_round"
val PREF_DEFAULT_SYNC_SERVER: String = "default_sync_server"
val PREF_DEFAULT_SYNC_SERVER_PASSWORD: String = "default_sync_server_password"
