package com.tsiemens.bridgescore

import com.tsiemens.bridgescore.orm.BridgeBoard
import java.util.*

fun duplicateBoardVulnerability(boardNo: Int): Vulnerability {
    return when((boardNo - 1) % 16) {
        0, 7, 10, 13 -> Vulnerability.none
        1, 4, 11, 14 -> Vulnerability.ns
        2, 5, 8, 15 -> Vulnerability.ew
        3, 6, 9, 12 -> Vulnerability.both
        else -> throw AssertionError(boardNo.toString())
    }
}

fun duplicateContractVulnerability(boardNumber: Int, declarer: Seat): Boolean {
    val vulTeams = duplicateBoardVulnerability(boardNumber)
    return vulTeams == Vulnerability.both || when(declarer) {
        Seat.none -> false
        Seat.north -> vulTeams == Vulnerability.ns
        Seat.east -> vulTeams == Vulnerability.ew
        Seat.south -> vulTeams == Vulnerability.ns
        Seat.west -> vulTeams == Vulnerability.ew
    }
}

fun duplicateContractVulnerability(board: BridgeBoard): Boolean {
    return duplicateContractVulnerability(board.boardNumber, board.declarer)
}

fun bridgeContractPoints(board: BridgeBoard): Int {
    val contractTricks = if(board.relativeTricks() >= 0) board.level else 0

    var contractTrickPoints =  when(board.suit) {
        Suit.none -> throw IllegalStateException()
        Suit.clubs, Suit.diamonds -> 20 * contractTricks
        Suit.hearts, Suit.spades -> 30 * contractTricks
        Suit.noTrumps -> if(contractTricks > 0) ( 30 * contractTricks ) + 10 else 0
    }
    if(board.doubled == Doubled.doubled) {
        contractTrickPoints *= 2
    } else if (board.doubled == Doubled.redoubled) {
        contractTrickPoints *= 4
    }
    return contractTrickPoints
}

fun bridgeOtherAboveLineScore(board: BridgeBoard, vulnerable: Boolean): Int {
    val relTricks = board.relativeTricks()
    val overTricks = if(relTricks > 0) relTricks else 0
    val underTricks = if(relTricks < 0) -1 * relTricks else 0

    var overTrickPoints: Int
    if(board.doubled == Doubled.undoubled) {
        overTrickPoints = when(board.suit) {
            Suit.none -> throw IllegalStateException()
            Suit.clubs, Suit.diamonds -> 20 * overTricks
            Suit.hearts, Suit.spades, Suit.noTrumps -> 30 * overTricks
        }
    } else if (board.doubled == Doubled.doubled) {
        overTrickPoints = 100 * overTricks
    } else {
        doAssert(board.doubled == Doubled.redoubled)
        overTrickPoints = 200 * overTricks
    }

    if(vulnerable &&
            (board.doubled == Doubled.doubled || board.doubled == Doubled.redoubled)) {
        overTrickPoints *= 2
    }

    var underTrickPoints: Int
    if(board.doubled == Doubled.undoubled) {
        // 50 for each undertrick
        underTrickPoints = -50 * underTricks
        if(vulnerable) {
            // 100 for each undertrick
            underTrickPoints *= 2
        }
    } else if(!vulnerable) {
        // 100 for first. 200 for 2nd and 3rd, 300 after that
        underTrickPoints = -100 * underTricks
        if(underTricks > 1) {
            underTrickPoints += -100 * (underTricks - 1)
        }
        if(underTricks > 3) {
            underTrickPoints += -100 * (underTricks - 3)
        }
    } else {
        // 200 for first. 300 after
        underTrickPoints = -200 * underTricks
        if(underTricks > 1) {
            underTrickPoints += -100 * (underTricks - 1)
        }
    }

    if(board.doubled == Doubled.redoubled) {
        underTrickPoints *= 2
    }

    var insult = 0
    if(underTricks == 0) {
        insult = when(board.doubled) {
            Doubled.undoubled -> 0
            Doubled.doubled -> 50
            Doubled.redoubled -> 100
        }
    }

    return overTrickPoints + underTrickPoints + insult
}

fun slamBonus(board: BridgeBoard, vul: Boolean): Int {
    var slamBonus = 0
    if(board.level == 7 && board.tricks == 13) {
        // Grand slam
        slamBonus = if(vul) 1500 else 1000
    } else if(board.level == 6 && board.tricks >= 12) {
        // Slam
        slamBonus = if(vul) 750 else 500
    }
    return slamBonus
}

fun duplicateBoardScore(board: BridgeBoard): Int {
    if(board.level == 0) return 0 // No bid made

    val vul = duplicateContractVulnerability(board)
    val contractPoints = bridgeContractPoints(board)

    var gameBonus = 0
    if(contractPoints >= 100) {
        gameBonus = if(vul) 500 else 300
    } else if(board.relativeTricks() >= 0) {
        gameBonus = 50
    }

    val slamBonus = slamBonus(board, vul)
    val total = contractPoints + bridgeOtherAboveLineScore(board, vul) + gameBonus + slamBonus
    return if(board.mySeat.clockwiseDistFrom(board.declarer) % 2 == 0) total else -1 * total
}

/**
 * Returns a map of board ids to matchpoints for that board
 */
fun boardMatchpoints(boards: List<BridgeBoard>): Map<Long, Float> {
    val boardMatchpoints = HashMap<Long, Float>()
    if(boards.size == 0) return boardMatchpoints

    val boardNum = boards[0].boardNumber
    val boardPoints = ArrayList<Pair<Long, Int>>()
    for(board in boards) {
        doAssert(board.boardNumber == boardNum)
        boardPoints.add(Pair(board.id!!, duplicateBoardScore(board)))
    }
    boardPoints.sortBy { p -> p.second }

    var firstWithScore = 0
    var lastWithScore = -1
    var points = boardPoints[0].second
    for(i in 0..boardPoints.size - 1) {
        if(i == boardPoints.size - 1 || boardPoints[i + 1].second > points) {
            lastWithScore = i
            if(i < boardPoints.size - 1) {
                points = boardPoints[i + 1].second
            }
        }
        if(lastWithScore >= firstWithScore) {
            // Crossing the boundary
            for(j in firstWithScore..lastWithScore) {
                boardMatchpoints.put(boardPoints[j].first,
                        firstWithScore + (0.5 * (lastWithScore - firstWithScore)).toFloat())
            }
            firstWithScore = i + 1
        }
    }
    return boardMatchpoints
}

/**
 * Calculate the IMP score for a duplicate score point difference.
 *
 * The ranges are based on this table (from http://www.math.cornell.edu/~belk/impmp.htm)
 *
 * Diff. in Pts.	    IMPs
 * 20 - 40  	1
 * 50 - 80  	2
 * 90 - 120  	3
 * 130 - 160  	4
 * 170 - 210  	5
 * 220 - 260  	6
 * 270 - 310  	7
 * 320 - 360  	8
 * 370 - 420  	9
 * 430 - 490  	10
 * 500 - 590  	11
 * 600 - 740  	12
 * 750 - 890 	13
 * 900 - 1090 	14
 * 1100 - 1290 	15
 * 1300 - 1490 	16
 * 1500 - 1740 	17
 * 1750 - 1990 	18
 * 2000 - 2240	19
 * 2250 - 2490	20
 * 2500 - 2990	21
 * 3000 - 3490	22
 * 3500 - 3990	23
 * 4000 and up	24
 *
 */

private val IMP_RANGE_STARTS = intArrayOf(0, 20, 50, 90, 130, 170, 220, 270, 320, 370, 430,
        500, 600, 750, 900, 1100, 1300, 1500, 1750, 2000, 2250, 2500, 3000, 3500, 4000)

private fun binSearchInImpRanges(v: Int, arr: IntArray): Int {
    var upperI = arr.size
    var lowerI = 0

    var i: Int = lowerI + ((upperI - lowerI) / 2)

    while(true) {
        if(v < arr[i]) {
            upperI = i
        } else {
            lowerI = i
        }

        if((upperI - lowerI) == 1) {
            return lowerI
        } else {
            i = lowerI + ((upperI - lowerI) / 2)
        }
    }
}

fun scoreToIMPs(score: Int): Int {
    val imp = binSearchInImpRanges(Math.abs(score), IMP_RANGE_STARTS)
    return if(score >= 0) imp else -1 * imp
}

fun boardIMP(boardScore1: Int, boardScore2: Int): Int {
    return scoreToIMPs(boardScore1 - boardScore2)
}

data class RubberBridgeBoardScore(val weBelow: Int, val weAbove: Int,
                                  val theyBelow: Int, val theyAbove: Int)

fun rubberBoardScore(board: BridgeBoard, vul: Boolean): RubberBridgeBoardScore {
    val contractPoints = bridgeContractPoints(board)

    val honors = when(board.honors) {
        Honors.none -> 0
        Honors.all -> 150
        Honors.partial -> 100
        Honors.allDef -> -150
        Honors.partialDef -> -100
    }
    val weDeclare = board.mySeat.clockwiseDistFrom(board.declarer) % 2 == 0

    var declAbove = if(honors > 0) honors else 0
    var defAbove = if(honors < 0) Math.abs(honors) else 0

    val slamBonus = slamBonus(board, vul)
    declAbove += slamBonus

    val aboveLine = bridgeOtherAboveLineScore(board, vul)
    if(aboveLine > 0) {
        declAbove += aboveLine
    } else {
        defAbove -= aboveLine
    }

    val weAbove = if(weDeclare) declAbove else defAbove
    val theyAbove = if(weDeclare) defAbove else declAbove

    val weBelow = if(weDeclare) contractPoints else 0
    val theyBelow = if(!weDeclare) contractPoints else 0
    doAssert(weBelow + theyBelow == contractPoints)

    return RubberBridgeBoardScore(weBelow = weBelow, weAbove = weAbove,
            theyBelow = theyBelow, theyAbove = theyAbove)
}

data class RubberBelowScore(val points: Int, val we: Boolean, val endOfGame: Boolean,
                            val endOfRubber: Boolean)

class RubberScoreboard {
    companion object {
        val PTS_TO_GAME = 100
    }

    val weAbove = ArrayList<Int>()
    val theyAbove = ArrayList<Int>()
    val below = ArrayList<RubberBelowScore>()
    val boardVuln = HashMap<Int, Boolean>()

    private var weGamesInRubber = 0
    private var theyGamesInRubber = 0

    private var wePtsToGame = PTS_TO_GAME
    private var theyPtsToGame = PTS_TO_GAME

    fun addBoard(board: BridgeBoard) {
        if (board.declarer == Seat.none) {
            doAssert(board.level == 0)
            return
        }
        val weDeclare = board.mySeat.clockwiseDistFrom(board.declarer) % 2 == 0
        val vul = (weDeclare && weGamesInRubber > 0) || (!weDeclare && theyGamesInRubber > 0)
        val scores = rubberBoardScore(board, vul)
        boardVuln.put(board.boardNumber, vul)

        if(scores.weAbove > 0) {
            weAbove.add(scores.weAbove)
        }
        if(scores.theyAbove > 0) {
            theyAbove.add(scores.theyAbove)
        }

        if(scores.weBelow > 0 || scores.theyBelow > 0) {
            wePtsToGame -= scores.weBelow
            theyPtsToGame -= scores.theyBelow

            val belowPoints = if (scores.weBelow > 0) scores.weBelow else scores.theyBelow
            val we = scores.weBelow > 0
            val endOfGame = wePtsToGame <= 0 || theyPtsToGame <= 0
            var endOfRubber = false

            // Cut off the legs and check for rubber
            if(endOfGame) {
                doAssert(scores.weBelow > 0 != scores.theyBelow > 0)
                wePtsToGame = PTS_TO_GAME
                theyPtsToGame = PTS_TO_GAME

                if(scores.weBelow > 0) {
                    weGamesInRubber++
                } else {
                    theyGamesInRubber++
                }

                if(weGamesInRubber == 2) {
                    weAbove.add(if(theyGamesInRubber == 0) 700 else 500)
                } else if(theyGamesInRubber == 2) {
                    theyAbove.add(if(weGamesInRubber == 0) 700 else 500)
                }

                if(weGamesInRubber == 2 || theyGamesInRubber == 2) {
                    weGamesInRubber = 0
                    theyGamesInRubber = 0
                    endOfRubber = true
                }
            }

            val belowScore = RubberBelowScore(belowPoints, we, endOfGame, endOfRubber)
            below.add(belowScore)
        }
    }
}

fun rubberScoreboard(boards: List<BridgeBoard>): RubberScoreboard {
    val scoreboard = RubberScoreboard()
    for(board in boards) {
        scoreboard.addBoard(board)
    }
    return scoreboard
}

fun dealerDirection(boardNo: Int): Direction {
    doAssert(boardNo > 0)
    return if(boardNo % 2 == 1) Direction.ns else Direction.ew
}

// ==========================================================================
// Par score logic, as specified by:
// One-Table Duplicate-Syle Par Score Tally logic
// http://www.compensationtable.com/One-Table%20Duplicate%20Tally%20Sheet.pdf
// ==========================================================================

fun scoringLinePair(boardNo: Int?, nsHcp: Int): Direction? {
    if (nsHcp < 0) {
        return null
    }
    val pair: Direction?
    var dealerDir: Direction? = null
    if (boardNo != null && boardNo > 0) {
        dealerDir = dealerDirection(boardNo)
    }

    // If HCP are equal, the dealer is the Scoring Line Pair
    if (nsHcp == 20) {
        pair = dealerDir
    } else if (nsHcp > 20) {
        pair = Direction.ns
    } else {
        pair = Direction.ew
    }
    return pair
}

/** Aka FitPars */
data class FPs(val nonVul: Int, val vul: Int)
/** Aka HighCardPointPars */
data class HCPPs(val maj8: FPs, val maj9: FPs, val maj10: FPs,
                 val min8: FPs, val min9: FPs, val min10: FPs,
                 val nf: FPs)

// Generated by scripts/parTableConverter.py
private val PAR_TABLE = arrayOf(
    // HCP
    /* 20 */ HCPPs(FPs(30, 10), FPs(90, 70), FPs(120, 120), FPs(-20, -40), FPs(-40, -60), FPs(-60, -70), FPs(20, -10)),
    /* 21 */ HCPPs(FPs(60, 30), FPs(140, 130), FPs(160, 240), FPs(30, 0), FPs(20, -10), FPs(40, 40), FPs(40, 20)),
    /* 22 */ HCPPs(FPs(100, 100), FPs(170, 200), FPs(230, 310), FPs(60, 50), FPs(60, 70), FPs(70, 100), FPs(60, 50)),
    /* 23 */ HCPPs(FPs(140, 180), FPs(260, 330), FPs(280, 380), FPs(80, 80), FPs(90, 110), FPs(160, 260), FPs(90, 80)),
    /* 24 */ HCPPs(FPs(190, 310), FPs(330, 450), FPs(370, 510), FPs(160, 180), FPs(170, 200), FPs(260, 320), FPs(160, 150)),
    /* 25 */ HCPPs(FPs(270, 410), FPs(390, 550), FPs(400, 570), FPs(190, 270), FPs(260, 400), FPs(320, 450), FPs(220, 290)),
    /* 26 */ HCPPs(FPs(350, 500), FPs(430, 630), FPs(440, 640), FPs(320, 440), FPs(340, 500), FPs(420, 560), FPs(310, 480)),
    /* 27 */ HCPPs(FPs(400, 580), FPs(470, 690), FPs(510, 730), FPs(360, 490), FPs(410, 590), FPs(480, 680), FPs(390, 580)),
    /* 28 */ HCPPs(FPs(420, 630), FPs(540, 750), FPs(640, 920), FPs(390, 580), FPs(470, 680), FPs(590, 870), FPs(420, 600)),
    /* 29 */ HCPPs(FPs(450, 660), FPs(600, 860), FPs(720, 1050), FPs(420, 640), FPs(530, 770), FPs(670, 1000), FPs(440, 630)),
    /* 30 */ HCPPs(FPs(550, 770), FPs(730, 1050), FPs(920, 1320), FPs(490, 740), FPs(670, 990), FPs(850, 1250), FPs(470, 660)),
    /* 31 */ HCPPs(FPs(620, 900), FPs(890, 1280), FPs(1020, 1470), FPs(560, 840), FPs(820, 1210), FPs(950, 1400), FPs(510, 710)),
    /* 32 */ HCPPs(FPs(770, 1120), FPs(970, 1400), FPs(1070, 1550), FPs(710, 1060), FPs(900, 1330), FPs(1000, 1480), FPs(640, 900)),
    /* 33 */ HCPPs(FPs(910, 1310), FPs(1010, 1460), FPs(1110, 1610), FPs(840, 1240), FPs(940, 1390), FPs(1040, 1540), FPs(820, 1170)),
    /* 34 */ HCPPs(FPs(1010, 1460), FPs(1110, 1610), FPs(1210, 1760), FPs(940, 1390), FPs(1040, 1540), FPs(1140, 1690), FPs(920, 1320)),
    /* 35 */ HCPPs(FPs(1110, 1610), FPs(1210, 1760), FPs(1310, 1910), FPs(1040, 1540), FPs(1140, 1690), FPs(1240, 1840), FPs(1020, 1470)),
    /* 36 */ HCPPs(FPs(1210, 1760), FPs(1310, 1910), FPs(1410, 2060), FPs(1140, 1690), FPs(1240, 1840), FPs(1340, 1990), FPs(1120, 1620)),
    /* 37 */ HCPPs(FPs(1310, 1910), FPs(1410, 2060), FPs(1510, 2210), FPs(1240, 1840), FPs(1340, 1990), FPs(1440, 2140), FPs(1220, 1770)),
    /* 38 */ HCPPs(FPs(1410, 2060), FPs(1510, 2210), FPs(1610, 2360), FPs(1340, 1990), FPs(1440, 2140), FPs(1540, 2290), FPs(1320, 1920)),
    /* 39 */ HCPPs(FPs(1510, 2210), FPs(1610, 2360), FPs(1710, 2510), FPs(1440, 2140), FPs(1540, 2290), FPs(1640, 2440), FPs(1420, 2070)),
    /* 40 */ HCPPs(FPs(1610, 2360), FPs(1710, 2510), FPs(1810, 2660), FPs(1540, 2290), FPs(1640, 2440), FPs(1740, 2590), FPs(1520, 2220))
)

fun getParScore(scoringLineHcp: Int, scoringLineFitLen: Int, scoringLineFitSuit: Suit,
             vulnerable: Boolean): Int {
    val parScoreRow = PAR_TABLE[scoringLineHcp - 20]
    val parScores = when(scoringLineFitSuit) {
        Suit.spades, Suit.hearts -> when(scoringLineFitLen) {
            8 -> parScoreRow.maj8
            9 -> parScoreRow.maj9
            else -> parScoreRow.maj10
        }
        Suit.diamonds, Suit.clubs -> when(scoringLineFitLen) {
            8 -> parScoreRow.min8
            9 -> parScoreRow.min9
            else -> parScoreRow.min10
        }
        Suit.none -> parScoreRow.nf
        else -> throw IllegalStateException()
    }
    return if(vulnerable) parScores.vul else parScores.nonVul
}

/**
 * Get the IMP score for a Par Scored board.
 * IMPs are negative if they are to be awarded to the opposing team.
 * NOTE: In this system, negative IMPs are not to be applied to the losing team.
 */
fun parScoreBoardIMPs(board: BridgeBoard): Int {
    doAssert(board.isValid())

    val scoringLineDir: Direction =
            scoringLinePair(board.boardNumber, board.nsHighCardPoints)!!

    val myDupScore = duplicateBoardScore(board)
    val declrScore: Int
    if (board.level > 0) {
        val iAmDeclarer = board.mySeat.clockwiseDistFrom(board.declarer) % 2 == 0
        declrScore = if(iAmDeclarer) myDupScore else -1 * myDupScore
    } else {
        declrScore = myDupScore
    }

    val scoringLineHcp = Math.max(board.nsHighCardPoints, 40 - board.nsHighCardPoints)
    val parScore = getParScore(scoringLineHcp, board.scoringLineFitLength, board.scoringLineFitSuit,
                               duplicateContractVulnerability(board))
    val scoringLinePlusMinus = if(scoringLineDir == board.declarer.direction()) 1 else -1
    val diff = (scoringLinePlusMinus * declrScore) - parScore

    val scoringLineImps = scoreToIMPs(diff)
    return if(scoringLineDir == board.mySeat.direction()) scoringLineImps else -1 * scoringLineImps
}
