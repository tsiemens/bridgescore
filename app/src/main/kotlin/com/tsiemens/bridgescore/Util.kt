package com.tsiemens.bridgescore

import android.content.Context
import android.provider.Settings
import java.math.BigInteger
import java.security.MessageDigest

fun doAssert(b: Boolean) {
    if(!b) {
        throw AssertionError()
    }
}

fun funcTag(): String {
    val st = Thread.currentThread().stackTrace
    if(st.size < 4) {
        return "(????)"
    } else {
        // Before is getThreadStackTrace, getStackTrace, funLogTag
        val ste = st.get(3)
        val nss = ste.className.split(".")
        return nss.get(nss.lastIndex) + "::" + ste.methodName
    }
}

fun getDeviceId(ctx: Context): String {
    var serialNo = ""
    try {
        val c = Class.forName("android.os.SystemProperties")
        serialNo = c.getMethod("get", String::class.java, String::class.java)
                .invoke(c, "ro.serialno", "unknown") as String
    } catch(e: Exception) {
        Trace.e(funcTag(), e.toString())
    }

    val androidId = Settings.Secure.getString(ctx.contentResolver, Settings.Secure.ANDROID_ID)
    Trace.d(funcTag(), "Android ID: " + androidId)
    Trace.d(funcTag(), "Serial No.: " + serialNo)
    val md = MessageDigest.getInstance("SHA-256")
    md.update(serialNo.toByteArray())
    md.update(androidId.toByteArray())
    return BigInteger(1, md.digest()).toString(16)
}

fun sha1HashHex(str: String): String {
    val md = MessageDigest.getInstance("SHA-1")
    md.update(str.toByteArray())
    return BigInteger(1, md.digest()).toString(16)
}

fun compareLong(lhs: Long, rhs: Long): Int {
    if(lhs > rhs) {
        return 1
    } else if (lhs < rhs) {
        return -1
    } else {
        return 0
    }
}