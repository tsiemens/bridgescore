package com.tsiemens.bridgescore

import android.content.Intent
import android.view.View
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.tsiemens.bridgescore.orm.BridgeBoard
import com.tsiemens.bridgescore.orm.BridgeBoardSet
import com.tsiemens.bridgescore.orm.BridgeSession
import com.tsiemens.bridgescore.orm.Orm
import com.tsiemens.bridgescore.ui.BoardListActivity
import com.tsiemens.bridgescore.ui.DuplicateBoardListActivity
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExternalResource
import org.junit.runner.Description
import org.junit.runner.RunWith
import org.junit.runners.model.Statement

@RunWith(AndroidJUnit4::class)
@LargeTest
class BoardListActivityTest {

    val sessionId = 1L
    val myBoardSetId = 2L
    var currentRound = "Round 4"

    inner class MyRule(
        private val inner: ActivityScenarioRule<DuplicateBoardListActivity>
    ) : ExternalResource() {

        override fun apply(base: Statement, description: Description): Statement =
            super.apply(inner.apply(base, description), description)

        override fun before() {
            // ... beforeActivityLaunched
            dataSetUp()
        }

        override fun after() {
            // ... afterActivityFinished
        }
    }

    fun getScenarioRuleIntent(): Intent {
        val intent = Intent(
            ApplicationProvider.getApplicationContext(),
            DuplicateBoardListActivity::class.java)
        intent.putExtra(BoardListActivity.BRIDGE_SESSION_ID, sessionId)
        return intent
    }

    @get:Rule
    val myRule = MyRule(ActivityScenarioRule<DuplicateBoardListActivity>(getScenarioRuleIntent()))

    fun dataSetUp() {
        Orm.ormWrapper = OrmMocksWrapper()
        OrmMockDb.boardTable.entries.clear()
        OrmMockDb.boardSetTable.entries.clear()
        OrmMockDb.sessTable.entries.clear()

        val sess = BridgeSession()
        sess.id = sessionId
        sess.boardsPerRound = 2
        sess.name = "Sess name"
        sess.mySeatDefault = Seat.north
        sess.type = ContractBridgeType.duplicate
        sess.timestamp = 100
        Orm.save(sess)

        val boardSet = BridgeBoardSet(sessionId = sessionId,
                teamName = "Me & Myself")
        boardSet.id = myBoardSetId
        Orm.save(boardSet)

        for(i in 2 .. 7) {
            val board = BridgeBoard()
            board.boardNumber = i
            board.declarer = Seat.north
            board.mySeat = Seat.south
            board.doubled = Doubled.undoubled
            board.level = 1
            board.suit = Suit.spades
            board.tricks = 1
            board.boardSetId = myBoardSetId
            Orm.save(board)
        }

        currentRound = "All Rounds"
    }

    private fun checkBoardItemsVisible(boardNums: IntRange, visible: Boolean = true) {
        for(i in boardNums) {
            onView(withText(i.toString())).check { view, noMatchingViewException ->
                if(visible) {
                    assertNotNull(view)
                } else {
                    assertNull(view)
                }
            }
        }
    }

    val viewVisible = { v: View?, e: NoMatchingViewException? -> assertNotNull(v) }
    val viewNotVisible = { v: View?, e: NoMatchingViewException? -> assertNull(v) }

    @Before
    fun sanityChecks() {
        checkBoardItemsVisible(2 .. 7)
        onView(withSpinnerText(currentRound)).check(viewVisible)
    }

    @After
    fun resetData() {
        dataSetUp()
    }

    private fun changeRoundSelected(round: Int) {
        val nextRound = if(round == 0) "All Rounds" else "Round " + round
        onView(withSpinnerText(currentRound)).perform(click())
        onView(withText(nextRound)).perform(click())
        onView(withSpinnerText(nextRound)).check(viewVisible)
        if(nextRound != currentRound) {
            onView(withSpinnerText(currentRound)).check(viewNotVisible)
            currentRound = nextRound
        }
    }

    @Test
    fun testCreateActivity() {
        changeRoundSelected(0)
        checkBoardItemsVisible(2 .. 7)
    }


    @Test
    fun testDeleteLast() {
        changeRoundSelected(4)
        onView(withText("7")).perform(click())
        onView(withId(R.id.delete)).perform(click())

        onView(withSpinnerText("Round 3")).check(viewVisible)
        checkBoardItemsVisible(7 .. 7, visible = false)
        checkBoardItemsVisible(2 .. 4, visible = false)
        checkBoardItemsVisible(5 .. 6)

        // FIXME because of perform wiating for "Idling resources" which includes
        // the snackbar, we can no longer click undo in these tests, because the system
        // is not completely idle while it is shown.
        // onView(withText("UNDO")).check(viewVisible)
        // Thread.sleep(6000)
        // onView(withText("UNDO")).check(viewNotVisible)

        currentRound = "Round 3"
        onView(withSpinnerText(currentRound)).check(viewVisible)
        checkBoardItemsVisible(7 .. 7, visible = false)
        checkBoardItemsVisible(2 .. 4, visible = false)
        checkBoardItemsVisible(5 .. 6)

        onView(withSpinnerText(currentRound)).perform(click())
        onView(withText("Round 4")).check(viewNotVisible)
    }

    // FIXME because of perform wiating for "Idling resources" which includes
    // the snackbar, we can no longer click undo in these tests, because the system
    // is not completely idle while it is shown.
    // @Test
    fun testDeleteLastUndo() {
        changeRoundSelected(4)
        onView(withText("7")).perform(click())
        onView(withId(R.id.delete)).perform(click())

        onView(withSpinnerText("Round 3")).check(viewVisible)
        checkBoardItemsVisible(7 .. 7, visible = false)
        checkBoardItemsVisible(2 .. 4, visible = false)
        checkBoardItemsVisible(5 .. 6)

        onView(withText("UNDO")).perform(click())

        currentRound = "Round 3"
        onView(withSpinnerText(currentRound)).check(viewVisible)
        checkBoardItemsVisible(7 .. 7, visible = false)
        checkBoardItemsVisible(2 .. 4, visible = false)
        checkBoardItemsVisible(5 .. 6)

        changeRoundSelected(4)
        checkBoardItemsVisible(7 .. 7)
    }

    @Test
    fun testDeleteMiddle() {
        changeRoundSelected(2)
        onView(withText("3")).perform(click())
        onView(withText("4")).perform(click())
        onView(withId(R.id.delete)).perform(click())

        onView(withSpinnerText("Round 4")).check(viewVisible)
        checkBoardItemsVisible(7 .. 7)
        checkBoardItemsVisible(2 .. 6, visible = false)
    }

    @Test
    fun testDeleteFromAllView() {
        changeRoundSelected(0)
        onView(withText("3")).perform(click())
        onView(withText("4")).perform(click())
        onView(withText("7")).perform(click())
        onView(withId(R.id.delete)).perform(click())

        onView(withSpinnerText("All Rounds")).check(viewVisible).perform(click())
        onView(withText("Round 4")).check(viewNotVisible)
        onView(withText("All Rounds")).perform(click()) // Dismiss the spinner
        checkBoardItemsVisible(2 .. 2)
        checkBoardItemsVisible(3 .. 4, visible = false)
        checkBoardItemsVisible(5 .. 5)
        checkBoardItemsVisible(7 .. 7, visible = false)
    }
}