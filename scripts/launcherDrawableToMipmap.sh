#!/bin/bash
# A utility to migrate standard exported launcher icons from the drawable directories
# to the mipmap directories

PROJECT_ROOT=$(git rev-parse --show-toplevel)
if [ $? != 0 ]; then
   echo Not in git repo
   exit 1
fi

for dpi in mdpi hdpi xhdpi xxhdpi xxxhdpi; do
   mv $PROJECT_ROOT/app/src/main/res/drawable-$dpi/ic_launcher.png  $PROJECT_ROOT/app/src/main/res/mipmap-$dpi/ic_launcher.png
done
